<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="e-commerce site well design with responsive view." />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" media="screen" />
    <link href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="{{asset('assets/css/stylesheet.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('assets/owl-carousel/owl.carousel.css')}}" type="text/css" rel="stylesheet" media="screen" />
    <link href="{{asset('assets/owl-carousel/owl.transitions.css')}}" type="text/css" rel="stylesheet" media="screen" />
    <script src="{{asset('assets/javascript/jquery-2.1.1.min.js')}}" type="text/javascript"></script>
{{--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap-theme.min.css">--}}
{{--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>--}}

    <script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/javascript/jstree.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/javascript/template.js')}}" type="text/javascript" ></script>
    <script src="{{asset('assets/javascript/common.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/javascript/global.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script>

{{--    <link href="{{asset('datetimepicker/css/bootstrap-datetimepicker.css')}}" rel="stylesheet"/>--}}
{{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>--}}
{{--    <script src="{{asset('datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>--}}
{{--    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">--}}
{{--    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>--}}
    @yield('bs')

</head>
<body>
<div class="preloader loader" style="display: block; background:#f2f2f2;"> <img src="{{asset('assets/image/loader.gif')}}"  alt="#"/></div>
@include('site.partials.header')
@include('site.partials.nav')
@yield('banner')
<div class="container">
    @yield('content')
</div>
@include('site.partials.footer')
@stack('scripts')
<script src="{{asset('assets/javascript/parally.js')}}"></script>
<script>
    $('.parallax').parally({offset: -40});
</script>
</body>
</html>
