@extends('site.layout.site')

@section('title', 'category')
@section('content')
    @if($slug === 'banh-mi')
        <ul class="breadcrumb">
            <li><a href="index.html"><i class="fa fa-home"></i></a></li>
            <li><a href="category.html">bánh mì</a></li>
        </ul>
    @endif
    @if($slug === 'xoi')
        <ul class="breadcrumb">
            <li><a href="index.html"><i class="fa fa-home"></i></a></li>
            <li><a href="category.html">xôi</a></li>
        </ul>
    @endif
    @if($slug === 'do-an-vat')
        <ul class="breadcrumb">
            <li><a href="index.html"><i class="fa fa-home"></i></a></li>
            <li><a href="category.html">đồ ăn vặt</a></li>
        </ul>
    @endif
    @if($slug === 'thuc-uong')
        <ul class="breadcrumb">
            <li><a href="index.html"><i class="fa fa-home"></i></a></li>
            <li><a href="category.html">thức uống</a></li>
        </ul>
    @endif
    <div class="row">
        <div id="column-left" class="col-sm-3 hidden-xs column-left">
            <div class="column-block">
                <div class="columnblock-title">Danh mục</div>
                <div class="category_block">
                    <ul class="box-category treeview-list treeview collapsable">
                        <li><a href="{{route('get_category_product','banh-mi')}}">Bánh mì</a></li>
                        <li><a href="{{route('get_category_product','xoi')}}">Xôi</a></li>
                        <li><a href="{{route('get_category_product','do-an-vat')}}">Đồ ăn vặt</a></li>
                        <li><a href="{{route('get_category_product','thuc-uong')}}">Thức uống</a></li>
                    </ul>
                </div>
            </div>

            <h3 class="productblock-title">Combo Breakfast</h3>
            <div class="row special-grid product-grid">
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                    <div class="product-thumb transition">
                        <div class="image product-imageblock"> <a href="#"><img src="image/product/5product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"></a>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                        <div class="caption product-detail">
                            <h4 class="product-name"> <a href="product.html" title="women's clothing">Clothing</a> </h4>
                            <p class="price product-price"> <span class="price-new">$254.00</span><span class="price-tax">Ex Tax: $210.00</span> </p>
                        </div>
                        <div class="button-group">
                            <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                            <button type="button" class="addtocart-btn">Add to Cart</button>
                            <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                    <div class="product-thumb transition">
                        <div class="image product-imageblock"> <a href="#"><img src="image/product/1product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"></a>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                        <div class="caption product-detail">
                            <h4 class="product-name"> <a href="product.html" title="women's clothing">Clothing</a> </h4>
                            <p class="price product-price"> <span class="price-new">$254.00</span><span class="price-tax">Ex Tax: $210.00</span> </p>
                        </div>
                        <div class="button-group">
                            <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                            <button type="button" class="addtocart-btn">Add to Cart</button>
                            <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                    <div class="product-thumb transition">
                        <div class="image product-imageblock"> <a href="#"><img src="image/product/2product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"></a>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                        <div class="caption product-detail">
                            <h4 class="product-name"> <a href="product.html" title="women's clothing">Clothing</a> </h4>
                            <p class="price product-price"> <span class="price-new">$254.00</span><span class="price-tax">Ex Tax: $210.00</span> </p>
                        </div>
                        <div class="button-group">
                            <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                            <button type="button" class="addtocart-btn">Add to Cart</button>
                            <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                    <div class="product-thumb transition">
                        <div class="image product-imageblock"> <a href="#"><img src="image/product/5product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"></a>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                        <div class="caption product-detail">
                            <h4 class="product-name"> <a href="product.html" title="women's clothing">Clothing</a> </h4>
                            <p class="price product-price"> <span class="price-new">$254.00</span><span class="price-tax">Ex Tax: $210.00</span> </p>
                        </div>
                        <div class="button-group">
                            <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                            <button type="button" class="addtocart-btn">Add to Cart</button>
                            <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item last">
                    <div class="product-thumb transition">
                        <div class="image product-imageblock"> <a href="#"><img src="image/product/4product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"></a>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                        <div class="caption product-detail">
                            <h4 class="product-name"> <a href="product.html" title="women's clothing">Clothing</a> </h4>
                            <p class="price product-price"> <span class="price-new">$254.00</span><span class="price-tax">Ex Tax: $210.00</span> </p>
                        </div>
                        <div class="button-group">
                            <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                            <button type="button" class="addtocart-btn">Add to Cart</button>
                            <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="content" class="col-sm-9">
            @if($slug === 'banh-mi')
                <h2 class="category-title">Bánh mì</h2>
            @endif
                @if($slug === 'xoi')
                    <h2 class="category-title">Xôi</h2>
                @endif
                @if($slug === 'do-an-vat')
                    <h2 class="category-title">Đồ ăn vặt</h2>
                @endif
                @if($slug === 'thuc-uong')
                    <h2 class="category-title">Thức uống</h2>
                @endif
            <div class="row category-banner">
                @if($slug === 'banh-mi')
                <div class="col-sm-12 category-desc">Bánh mì - Nét văn hóa đặc trưng của ẩm thực Việt.
                    Bánh mì được tôn vinh là một trong những món ăn đường phố ngon nhất và là món sand-wich ngon nhất trên thế giới. Thậm chí "Bánh mì" - (banh mi /ˈbɑːn miː/) đã vinh hạnh là một trong ba từ ngữ Việt Nam có trong dữ liệu từ điển Oxford, cùng với "Phở" (pho /fə ː/) và "Áo dài" - (ao dai /ˈaʊ ˌdʌɪ/),  tất cả đã đủ để chứng tỏ độ nổi tiếng và sức hút khó cưỡng của món ăn này.</div>
                    @endif
                    @if($slug === 'xoi')
                        <div class="col-sm-12 category-desc">Nhà văn Thạch Lam, khi miêu tả về xôi và người bán xôi trong thiên tùy bút Quà Hà Nội đã viết: ...xôi nồng mùi gạo nếp. Xôi đậu, xôi lạc, xôi vừng mỡ và dừa. Ồ, cái xôi vừng mỡ, nắm từng nắm con, ăn vừa ngậy vừa bùi. Mà có đắt gì đâu! Aên một, hai xu là đủ rồi. Mùa rét thì xôi nóng, hãy còn hơi bốc lên như sương mù, ăn vừa nóng người vừa chắc dạ.

                            Và có ai ngẫm nghĩ kỹ cái vị hành khô chưng mỡ ở trong bát ngô nếp non bung: hành giòn và thơm phức, những hạt ngô béo rưới chút nước mỡ trong... Ngô bung (xôi lúa) thì có nhiều hàng ngon, nhưng ngon nhất và đậm nhất là ngô bung của một bà già trên Yên Phụ</div>
                    @endif
                    @if($slug === 'do-an-vat')
                        <div class="col-sm-12 category-desc">This is quà vặt</div>
                    @endif
                    @if($slug === 'thuc-uong')
                        <div class="col-sm-12 category-desc">This is milk, sting, coke v.v</div>
                    @endif
            </div>
            <div class="category-page-wrapper">

            </div>
            <br>
            <div class="grid-list-wrapper">
                @foreach($products as $product)
                <div class="product-layout product-list col-xs-12">
                    <div class="product-thumb">
                        <div class="image product-imageblock"> <a href="{{route('get_detail_product',$product->slug)}}"> <img src="{{asset('image_upload/images/'.$product->image)}}" alt="" title="" class="img-responsive" style="width: 220px; height: 294px"> </a>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                        <div class="caption product-detail">
                            <h4 class="product-name"> <a href="product.html" title="lorem ippsum dolor dummy"> {{$product->title}} </a> </h4>
                            @if($product->discount > 0)
                            <p class="price product-price">
                                <span class="price-old" style="color: darkred"> {{number_format($product->price,'0',',','.')}} đ</span>
                               {{number_format($product->discount,'0',',','.')}} đ </p>
                            @else
                            <p class="price product-price">
                                {{number_format($product->price,'0',',','.')}} đ </p>
                            @endif
                            <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
                        </div>
                        <div class="button-group">
                            <input type="hidden" name="quantity" value="1" size="2" >
                            <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                            <button type="button" class="addtocart-btn" data-toggle="modal" data-target="#modalDetaiProduct" data-id="{{$product->product_id}}" onclick="return addToCart(this);">Thêm vào giỏ</button>
                            <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                        </div>
                    </div>
                </div>
                <div class="clearfix visible-lg"></div>
                @endforeach
            </div>
{{--            <div class="category-page-wrapper">--}}
{{--                <div class="result-inner">Showing 1 to 8 of 10 (2 Pages)</div>--}}
{{--                <div class="pagination-inner">--}}
{{--                    <ul class="pagination">--}}
{{--                        <li class="active"><span>1</span></li>--}}
{{--                        <li><a href="category.html">2</a></li>--}}
{{--                        <li><a href="category.html">&gt;</a></li>--}}
{{--                        <li><a href="category.html">&gt;|</a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
    {{--    modal notifi add cart--}}
    <div class="modal fade" id="modalNotifi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width:300px;">
            <div class="modal-content">
                <div class="modal-body" id="modal-content">
                    <h4>Đã thêm vào giỏ hàng</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tiếp tục mua</button>
                    <a href="{{route('my_cart')}}" target="_self"><button type="button" class="btn btn-primary">Đi tới giỏ hàng</button></a>
                </div>
            </div>
        </div>
    </div>
    {{--    end modal--}}
@endsection
@push('scripts')
    <script type="text/javascript">
        function addToCart(e){
            var product_id = $(e).data('id');
            var quantity = $("input[name='quantity']").val();
            $.ajax({
                    url:'{{route('add_to_cart')}}',
                    method:'POST',
                    data:{
                        _token: "{{ csrf_token() }}",
                        product_id:product_id,
                        quantity:quantity
                    },
                    success: function (data) {
                        console.log(data);
                        $("#modalNotifi").modal('show');
                        $.ajax({
                            url:'{{route('update_cart')}}',
                            method:'GET',
                            success: function (data) {
                                $('#header-cart').empty();
                                $('#header-cart').html(data);
                                console.log(data);
                            },
                            error: function(error) {
                            }
                        })
                    },
                    error: function(error) {
                    }
                },
            );
        }


    </script>
@endpush
