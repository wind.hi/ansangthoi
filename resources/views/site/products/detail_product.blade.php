@extends('site.layout.site')

@section('title', 'product')
@section('content')
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
        <li><a href="category.html">Desktops</a></li>
        <li><a href="#">lorem ippsum dolor dummy</a></li>
    </ul>
    <div class="row">
        <div id="column-left" class="col-sm-3 hidden-xs column-left">
            <div class="column-block">
                <div class="column-block">
                    <div class="columnblock-title">Categories</div>
                    <div class="category_block">
                        <ul class="box-category treeview-list treeview collapsable">
                            <li class="expandable"><div class="hitarea expandable-hitarea"></div><a href="#" class="activSub">Desktops</a>
                                <ul class="collapsable" style="display: none;">
                                    <li><a href="#">PC</a></li>
                                    <li class="last"><a href="#">MAC</a></li>
                                </ul>
                            </li>
                            <li class="expandable"><div class="hitarea expandable-hitarea"></div><a href="#" class="activSub">Laptops &amp; Notebooks</a>
                                <ul class="collapsable" style="display: none;">
                                    <li><a href="#">Macs</a></li>
                                    <li class="last"><a href="#">Windows</a></li>
                                </ul>
                            </li>
                            <li class="expandable"><div class="hitarea expandable-hitarea"></div><a href="#" class="activSub">Components</a>
                                <ul class="collapsable" style="display: none;">
                                    <li><a href="#">Mice and Trackballs</a></li>
                                    <li class="expandable"><div class="hitarea expandable-hitarea"></div><a href="#" class="activSub">Monitors</a>
                                        <ul class="collapsable" style="display: none;">
                                            <li><a href="#">test 1</a></li>
                                            <li class="last"><a href="#">test 2</a></li>
                                        </ul>
                                    </li>
                                    <li class="last"><a href="#">Windows</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Tablets</a></li>
                            <li><a href="#">Software</a></li>
                            <li><a href="#">Phones &amp; PDAs</a></li>
                            <li><a href="#">Cameras</a></li>
                            <li class="last"><a href="#">MP3 Players</a></li>
                        </ul>
                    </div>
                </div>
                <h3 class="productblock-title">Bestsellers</h3>
                <div class="row bestseller-grid product-grid">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"> <a href="#"> <img src="image/product/2product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"> </a>
                                <div class="button-group">
                                    <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                    <button type="button" class="addtocart-btn">Add to Cart</button>
                                    <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name"> <a href="product.html" title="women's clothing">Clothing</a> </h4>
                                <p class="price product-price"> <span class="price-new">$254.00</span><span class="price-tax">Ex Tax: $210.00</span> </p>
                            </div>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"> <a href="#"> <img src="image/product/3product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"> </a>
                                <div class="button-group">
                                    <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                    <button type="button" class="addtocart-btn">Add to Cart</button>
                                    <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name"> <a href="product.html" title="women's clothing">Clothing</a> </h4>
                                <p class="price product-price"> <span class="price-new">$254.00</span><span class="price-tax">Ex Tax: $210.00</span> </p>
                            </div>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item last">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"> <a href="#"> <img src="image/product/4product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"> </a>
                                <div class="button-group">
                                    <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                    <button type="button" class="addtocart-btn">Add to Cart</button>
                                    <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name"> <a href="product.html" title="women's clothing">Clothing</a> </h4>
                                <p class="price product-price"> <span class="price-new">$254.00</span><span class="price-tax">Ex Tax: $210.00</span> </p>
                            </div>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <h3 class="productblock-title">Latest</h3>
                <div class="row latest-grid product-grid">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock">
                                <a href="#">
                                    <img src="image/product/1product50x59.jpg" alt="lorem ippsum dolor dummy" title="lorem ippsum dolor dummy" class="img-responsive">
                                </a>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name">
                                    <a href="#" title="lorem ippsum dolor dummy">Clothing</a>
                                </h4>
                                <p class="price product-price">$122.00<span class="price-tax">Ex Tax: $100.00</span></p>
                                <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"><a href="#"><img src="image/product/2product50x59.jpg" alt="lorem ippsum dolor dummy" title="lorem ippsum dolor dummy" class="img-responsive"></a>
                                <div class="button-group">
                                    <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                    <button type="button" class="addtocart-btn">Add to Cart</button>
                                    <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name">
                                    <a href="#" title="lorem ippsum dolor dummy">Clothing</a>
                                </h4>
                                <p class="price product-price">$122.00<span class="price-tax">Ex Tax: $100.00</span></p>
                                <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"><a href="#"><img src="image/product/3product50x59.jpg" alt="lorem ippsum dolor dummy" title="lorem ippsum dolor dummy" class="img-responsive"></a>
                                <div class="button-group">
                                    <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                    <button type="button" class="addtocart-btn">Add to Cart</button>
                                    <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name">
                                    <a href="#" title="lorem ippsum dolor dummy">Clothing</a>
                                </h4>
                                <p class="price product-price">$122.00<span class="price-tax">Ex Tax: $100.00</span></p>
                                <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item last">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"><a href="#"><img src="image/product/2product50x59.jpg" alt="lorem ippsum dolor dummy" title="lorem ippsum dolor dummy" class="img-responsive"></a>
                                <div class="button-group">
                                    <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                    <button type="button" class="addtocart-btn">Add to Cart</button>
                                    <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name">
                                    <a href="#" title="lorem ippsum dolor dummy">Clothing</a>
                                </h4>
                                <p class="price product-price">$122.00<span class="price-tax">Ex Tax: $100.00</span></p>
                                <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h3 class="productblock-title">Specials</h3>
                <div class="row special-grid product-grid">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"> <a href="#"><img src="image/product/5product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"></a>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name"><a href="#" title="lorem ippsum dolor dummy">Clothing</a></h4>
                                <p class="price product-price"> <span class="price-new">$254.00</span> <span class="price-tax">Ex Tax: $210.00</span> </p>
                            </div>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"> <a href="#"><img src="image/product/6product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"></a>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name"><a href="#" title="lorem ippsum dolor dummy">Clothing</a></h4>
                                <p class="price product-price"> <span class="price-new">$254.00</span> <span class="price-tax">Ex Tax: $210.00</span> </p>
                            </div>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"> <a href="#"><img src="image/product/7product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"></a>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name"><a href="#" title="lorem ippsum dolor dummy">Clothing</a></h4>
                                <p class="price product-price"> <span class="price-new">$254.00</span> <span class="price-tax">Ex Tax: $210.00</span> </p>
                            </div>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item last">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"> <a href="#"><img src="image/product/6product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"></a>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name"><a href="#" title="lorem ippsum dolor dummy">Clothing</a></h4>
                                <p class="price product-price"> <span class="price-new">$254.00</span> <span class="price-tax">Ex Tax: $210.00</span> </p>
                            </div>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="content" class="col-sm-9">
            <div class="row">
                <div class="col-sm-6">
                    <div class="thumbnails">
                        <div><a class="thumbnail" href="{{asset('image_upload/images/'.$product->image)}}" title="lorem ippsum dolor dummy"><img src="{{asset('image_upload/images/'.$product->image)}}" title="lorem ippsum dolor dummy" alt="lorem ippsum dolor dummy"></a></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <h1 class="productpage-title">{{$product->title}}</h1>
                    <div class="rating product"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span> <span class="review-count"> <a href="#" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">1 reviews</a> / <a href="#" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">Write a review</a></span>
                        <hr>
                        <!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                        <!-- AddThis Button END -->
                    </div>
                    <ul class="list-unstyled productinfo-details-top">
                        <li>
                            <h2 class="productpage-price">{{number_format($product->price,'0',',','.')}} vnđ</h2>
                        </li>
                        @if($product->discount >0)
                        <li><span class="price-old" style="color: #8B0000">{{number_format($product->discount,'0',',','.')}} vnđ </span></li>
                            @endif
                    </ul>
                    <hr>
                    <ul class="list-unstyled product_info">
                        <li>
                            <label>Danh mục: </label>
                            <span> <a href="#">{{$product->category}}</a></span></li>
                        <li>
                            <label>Mã sản phẩm: </label>
                            <span> {{$product->code}}</span></li>

                    </ul>
                    <hr>

                    <div id="product">
                        <div class="form-group">
                            <label class="control-label qty-label" for="input-quantity">Qty</label>
                            <input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control productpage-qty">
                            <input type="hidden" name="product_id" value="48">
                            <div class="btn-group">
                                <button type="button" data-toggle="tooltip" class="btn btn-default wishlist" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" id="button-cart" data-loading-text="Loading..." class="btn btn-primary btn-lg btn-block addtocart" data-id="{{$product->product_id}}" onclick="return addToCart(this);">Add to Cart</button>
                                <button type="button" data-toggle="tooltip" class="btn btn-default compare" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="productinfo-tab">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-description" data-toggle="tab">Description</a></li>
                    <li><a href="#tab-review" data-toggle="tab">Reviews ({{$reviews->count()}})</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-description">
                        <div class="cpt_product_description ">
                            <div>
                               {!! $product->description !!}
                            </div>
                        </div>
                        <!-- cpt_container_end --></div>
                    <div class="tab-pane" id="tab-review">
                        <div id="list-review">
                            @foreach($reviews as $review)
                            <ul class="blog-meta">
                                <li><i class="fa fa-user"></i><span><a rel="author" title="Posts by Admin" href="#">{{$review->customer_name}}</a></span></li>
                                <li><i class="fa fa-clock-o"></i><span class="dt-published">December 23, 2015</span></li>
                            </ul>
                            <p class="p-summary"></p>
                            <p>{{$review->content}}</p>
                            <hr/>
                                @endforeach
                        </div>

                        <form class="form-horizontal">
                            <div id="review"></div>
                            <h2>Write a review</h2>
                            <div class="form-group required">
                                <div class="col-sm-12">
                                    <label class="control-label" for="input-name">Your Name</label>
                                    <input type="text" name="customer_name" value="" id="input-name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group required">
                                <div class="col-sm-12">
                                    <label class="control-label" for="input-review">Your Review</label>
                                    <textarea name="content" rows="5" id="input-review" class="form-control"></textarea>
                                    <div class="help-block"><span class="text-danger">Note:</span> HTML is not translated!</div>
                                </div>
                            </div>
                            <div class="form-group required">
                                <div class="col-sm-12">
                                    <label class="control-label">Rating</label>
                                    &nbsp;&nbsp;&nbsp; Bad&nbsp;
                                    <input type="radio" name="rating" value="1">
                                    &nbsp;
                                    <input type="radio" name="rating" value="2">
                                    &nbsp;
                                    <input type="radio" name="rating" value="3">
                                    &nbsp;
                                    <input type="radio" name="rating" value="4">
                                    &nbsp;
                                    <input type="radio" name="rating" value="5">
                                    &nbsp;Good</div>

                            </div>
                            <div class="buttons clearfix">
                                <div class="pull-right">
                                    <button type="button" id="button-review" data-loading-text="Loading..." class="btn btn-primary" data-id="{{$product->post_id}}" onclick="return rate(this)">Đánh giá</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <h3 class="productblock-title">Related Products</h3>
            <div class="box">
                <div id="related-slidertab" class="row owl-carousel product-slider owl-theme" style="opacity: 1; display: block;">
                    <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 3360px; left: 0px; display: block;"><div class="owl-item" style="width: 240px;"><div class="item">
                                    <div class="product-thumb transition">
                                        <div class="image product-imageblock"> <a href="#"> <img src="image/product/pro-1-220x294.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"> </a>
                                            <div class="button-group">
                                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                            </div>
                                        </div>
                                        <div class="caption product-detail">
                                            <h4 class="product-name"><a href="product.html" title="women's clothing">women's clothing</a></h4>
                                            <p class="price product-price"> <span class="price-new">$254.00</span> <span class="price-old">$272.00</span> <span class="price-tax">Ex Tax: $210.00</span> </p>
                                        </div>
                                        <div class="button-group">
                                            <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                            <button type="button" class="addtocart-btn">Add to Cart</button>
                                            <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                        </div>
                                    </div>
                                </div></div><div class="owl-item" style="width: 240px;"><div class="item">
                                    <div class="product-thumb transition">
                                        <div class="image product-imageblock"> <a href="#"> <img src="image/product/pro-2-220x294.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"> </a>
                                            <div class="button-group">
                                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                            </div>
                                        </div>
                                        <div class="caption product-detail">
                                            <h4 class="product-name"><a href="product.html" title="women's clothing">women's clothing</a></h4>
                                            <p class="price product-price"> <span class="price-new">$254.00</span> <span class="price-old">$272.00</span> <span class="price-tax">Ex Tax: $210.00</span> </p>
                                        </div>
                                        <div class="button-group">
                                            <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                            <button type="button" class="addtocart-btn">Add to Cart</button>
                                            <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                        </div>
                                    </div>
                                </div></div><div class="owl-item" style="width: 240px;"><div class="item">
                                    <div class="product-thumb transition">
                                        <div class="image product-imageblock"> <a href="#"> <img src="image/product/pro-3-220x294.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"> </a>
                                            <div class="button-group">
                                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                            </div>
                                        </div>
                                        <div class="caption product-detail">
                                            <h4 class="product-name"><a href="product.html" title="women's clothing">women's clothing</a></h4>
                                            <p class="price product-price"> <span class="price-new">$254.00</span> <span class="price-old">$272.00</span> <span class="price-tax">Ex Tax: $210.00</span> </p>
                                        </div>
                                        <div class="button-group">
                                            <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                            <button type="button" class="addtocart-btn">Add to Cart</button>
                                            <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                        </div>
                                    </div>
                                </div></div><div class="owl-item" style="width: 240px;"><div class="item">
                                    <div class="product-thumb transition">
                                        <div class="image product-imageblock"> <a href="#"> <img src="image/product/pro-4-220x294.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"> </a>
                                            <div class="button-group">
                                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                            </div>
                                        </div>
                                        <div class="caption product-detail">
                                            <h4 class="product-name"><a href="product.html" title="women's clothing">women's clothing</a></h4>
                                            <p class="price product-price"> <span class="price-new">$254.00</span> <span class="price-old">$272.00</span> <span class="price-tax">Ex Tax: $210.00</span> </p>
                                        </div>
                                        <div class="button-group">
                                            <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                            <button type="button" class="addtocart-btn">Add to Cart</button>
                                            <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                        </div>
                                    </div>
                                </div></div><div class="owl-item" style="width: 240px;"><div class="item">
                                    <div class="product-thumb transition">
                                        <div class="image product-imageblock"> <a href="#"> <img src="image/product/pro-5-220x294.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"> </a>
                                            <div class="button-group">
                                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                            </div>
                                        </div>
                                        <div class="caption product-detail">
                                            <h4 class="product-name"><a href="product.html" title="women's clothing">women's clothing</a></h4>
                                            <p class="price product-price"> <span class="price-new">$254.00</span> <span class="price-old">$272.00</span> <span class="price-tax">Ex Tax: $210.00</span> </p>
                                        </div>
                                        <div class="button-group">
                                            <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                            <button type="button" class="addtocart-btn">Add to Cart</button>
                                            <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                        </div>
                                    </div>
                                </div></div><div class="owl-item" style="width: 240px;"><div class="item">
                                    <div class="product-thumb transition">
                                        <div class="image product-imageblock"> <a href="#"> <img src="image/product/pro-6-220x294.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"> </a>
                                            <div class="button-group">
                                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                            </div>
                                        </div>
                                        <div class="caption product-detail">
                                            <h4 class="product-name"><a href="product.html" title="women's clothing">women's clothing</a></h4>
                                            <p class="price product-price"> <span class="price-new">$254.00</span> <span class="price-old">$272.00</span> <span class="price-tax">Ex Tax: $210.00</span> </p>
                                        </div>
                                        <div class="button-group">
                                            <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                            <button type="button" class="addtocart-btn">Add to Cart</button>
                                            <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                        </div>
                                    </div>
                                </div></div><div class="owl-item" style="width: 240px;"><div class="item">
                                    <div class="product-thumb transition">
                                        <div class="image product-imageblock"> <a href="#"> <img src="image/product/pro-7-220x294.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"> </a>
                                            <div class="button-group">
                                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                            </div>
                                        </div>
                                        <div class="caption product-detail">
                                            <h4 class="product-name"><a href="product.html" title="women's clothing">women's clothing</a></h4>
                                            <p class="price product-price"> <span class="price-new">$254.00</span> <span class="price-old">$272.00</span> <span class="price-tax">Ex Tax: $210.00</span> </p>
                                        </div>
                                        <div class="button-group">
                                            <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                            <button type="button" class="addtocart-btn">Add to Cart</button>
                                            <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                        </div>
                                    </div>
                                </div></div></div></div>






                    <div class="owl-controls clickable"><div class="owl-buttons"><div class="owl-prev">prev</div><div class="owl-next">next</div></div></div></div>
            </div>
        </div>
    </div>
    {{--    modal notifi add cart--}}
    <div class="modal fade" id="modalNotifi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width:300px;">
            <div class="modal-content">
                <div class="modal-body" id="modal-content">
                    <h4>Đã thêm vào giỏ hàng</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tiếp tục mua</button>
                    <a href="{{route('my_cart')}}" target="_self"><button type="button" class="btn btn-primary">Đi tới giỏ hàng</button></a>
                </div>
            </div>
        </div>
    </div>
    {{--    end modal--}}
@endsection
@push('scripts')
    <script type="text/javascript">
        function addToCart(e){
            var product_id = $(e).data('id');
            var quantity = $("input[name='quantity']").val();
            $.ajax({
                    url:'{{route('add_to_cart')}}',
                    method:'POST',
                    data:{
                        _token: "{{ csrf_token() }}",
                        product_id:product_id,
                        quantity:quantity
                    },
                    success: function (data) {
                        console.log(data);
                        $("#modalNotifi").modal('show');
                        $.ajax({
                            url:'{{route('update_cart')}}',
                            method:'GET',
                            success: function (data) {
                                $('#header-cart').empty();
                                $('#header-cart').html(data);
                                console.log(data);
                            },
                            error: function(error) {
                            }
                        })
                    },
                    error: function(error) {
                    }
                },
            );
        }
    </script>

    <script type="text/javascript">
        function rate(e){
            var post_id = $(e).data('id');
            var customer_name = $("input[name='customer_name']").val();
            var content = $("textarea[name='content']").val();
            $.ajax({
                    url:'{{route('add_review')}}',
                    method:'POST',
                    data:{
                        _token: "{{ csrf_token() }}",
                        post_id:post_id,
                        customer_name:customer_name,
                        content:content
                    },
                    success: function (data) {
                        console.log(data);
                        $("#list-review").empty();
                        $("#list-review").html(data);
                    },
                    error: function(error) {
                    }
                },
            );
        }
    </script>
@endpush
