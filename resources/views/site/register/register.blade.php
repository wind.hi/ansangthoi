@extends('site.layout.site')

@section('title', 'register')
@section('content')
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Account</a></li>
        <li><a href="register.html">Register</a></li>
    </ul>
    <div class="row">
        <div class="col-sm-3 hidden-xs column-left" id="column-left">
            <div class="column-block">
                <div class="columnblock-title">Account</div>
                <div class="account-block">
                    <div class="list-group"> <a class="list-group-item" href="login.html">Login</a> <a class="list-group-item" href="register.html">Register</a> <a class="list-group-item" href="forgetpassword.html">Forgotten Password</a> <a class="list-group-item" href="#">My Account</a> <a class="list-group-item" href="#">Address Book</a> <a class="list-group-item" href="#">Wish List</a> <a class="list-group-item" href="#">Order History</a> <a class="list-group-item" href="download">Downloads</a> <a class="list-group-item" href="#">Reward Points</a> <a class="list-group-item" href="#">Returns</a> <a class="list-group-item" href="#">Transactions</a> <a class="list-group-item" href="#">Newsletter</a><a class="list-group-item last" href="#">Recurring payments</a> </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9" id="content">
            <h1>Đăng ký tài khoản</h1>
            <p>If you already have an account with us, please login at the <a href="login">login page</a>.</p>
            @if (session('status'))
                <ul>
                    <li class="text-danger"> {{ session('status') }}</li>
                </ul>
            @endif
            <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('customer.store')}}">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <fieldset id="account">
                    <legend>Nhập thông tin đăng ký</legend>
                    <div style="display: none;" class="form-group required">
                        <label class="col-sm-2 control-label">Customer Group</label>
                        <div class="col-sm-10">
                            <div class="radio">
                                <label>
                                    <input type="radio" checked="checked" value="1" name="customer_group_id">
                                    Default</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label for="input-firstname" class="col-sm-2 control-label">Họ tên</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-firstname" placeholder="Họ tên" value="" name="customer_name">
                        </div>
                    </div>
                    @if ($errors->has('customer_name'))
                        <span class="help-block">
                                         <strong>{{ $errors->first('customer_name') }}</strong>
                                        </span>
                    @endif
                    <div class="form-group required">
                        <label for="input-fax" class="col-sm-2 control-label">Địa chỉ</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-fax" placeholder="Địa chỉ" value="" name="address">
                        </div>
                    </div>
                    @if ($errors->has('address'))
                        <span class="help-block">
                                         <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                    @endif
                    <div class="form-group required">
                        <label for="input-lastname" class="col-sm-2 control-label">Nick Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="input-lastname" placeholder="Nick name" value="" name="nick_name">
                        </div>
                    </div>
                    @if ($errors->has('nick_name'))
                        <span class="help-block">
                                         <strong>{{ $errors->first('nick_name') }}</strong>
                                        </span>
                    @endif
                    <div class="form-group required">
                        <label for="input-email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="input-email" placeholder="E-Mail" value="" name="email">
                        </div>
                    </div>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                         <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                    @endif
                    <div class="form-group required">
                        <label for="input-password" class="col-sm-2 control-label">Mật khẩu</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="input-password" placeholder="Password" value="" name="password">
                        </div>
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                                         <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                    @endif

                </fieldset>

                <div class="buttons">
                    <div class="pull-right">I have read and agree to the <a class="agree" href="#"><b>Privacy Policy</b></a>
                        <input type="checkbox" value="1" name="agree">
                        &nbsp;
                        <input type="submit" class="btn btn-primary" value="Continue">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
