<header>
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="top-left pull-left">
                        <div class="language">
                            <form action="#" method="post" enctype="multipart/form-data" id="language">
                                <div class="btn-group">
                                    <button class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <img src="image/flags/gb.png" alt="English" title="English">English <i class="fa fa-caret-down"></i></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#"><img src="image/flags/lb.png" alt="Arabic" title="Arabic"> Arabic</a></li>
                                        <li><a href="#"><img src="image/flags/gb.png" alt="English" title="English"> English</a></li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                        <div class="currency">
                            <form action="#" method="post" enctype="multipart/form-data" id="currency">
                                <div class="btn-group">
                                    <button class="btn btn-link dropdown-toggle" data-toggle="dropdown"> <strong>$</strong> <i class="fa fa-caret-down"></i> </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <button class="currency-select btn btn-link btn-block" type="button" name="EUR">€ Euro</button>
                                        </li>
                                        <li>
                                            <button class="currency-select btn btn-link btn-block" type="button" name="GBP">£ Pound Sterling</button>
                                        </li>
                                        <li>
                                            <button class="currency-select btn btn-link btn-block" type="button" name="USD">$ US Dollar</button>
                                        </li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="top-right pull-right">
                        <div id="top-links" class="nav pull-right">
                            @if(\Illuminate\Support\Facades\Auth::check())
                                <?php
                                $customer = \App\Entity\Customer::where('customer_user_id', Auth::id())->first();
                                ?>
                            <ul class="list-inline">
                                <li class="dropdown"><a href="#" title="My Account" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-user"></i><span>Hi {{$customer->nick_name}}</span> <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="register.html">Tài Khoản</a></li>
                                        <li><a href="{{route('logout')}}">Đăng Xuất</a></li>
                                    </ul>
                                </li>
                                <li><a href="#" id="wishlist-total" title="Wish List (0)"><i class="fa fa-heart"></i><span>Wish List</span><span> (0)</span></a></li>
                            </ul>
                            @else
                                <ul class="list-inline">
                                    <li class="dropdown"><a href="#" title="My Account" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-user"></i><span>My Account</span> <span class="caret"></span></a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="register.html">Register</a></li>
                                            <li><a href="{{route('login_customer')}}">Login</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#" id="wishlist-total" title="Wish List (0)"><i class="fa fa-heart"></i><span>Wish List</span><span> (0)</span></a></li>
                                </ul>
                                @endif
                            <div class="search-box">
                                <input class="input-text" placeholder="search.." type="text">
                                <button class="search-btn"><i class="fa fa-search"></i></button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="header-inner">
            <div class="col-sm-4 col-xs-6 header-left">
                <div class="shipping">
                    <div class="shipping-img"></div>
                    <div class="shipping-text">+91(000)1234-1234<span class="shipping-detail">Week From 9:00am To 7:00pm</span></div>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12 header-middle">
                <div class="header-middle-top">
                    <div id="logo"> <a href="index.html"><img src="image/logo.png" title="E-Commerce" alt="E-Commerce" class="img-responsive" /></a> </div>
                </div>
            </div>
            @if(session('cart_item'))
                <?php
                $items = Session::get('cart_item');
                ?>
            <div class="col-sm-4 col-xs-12 header-right" id="header-cart">
                <div id="cart" class="btn-group btn-block">
                    <button type="button" class="btn btn-inverse btn-block btn-lg dropdown-toggle cart-dropdown-button"> <span id="cart-total"><span class="cart-title">Shopping Cart</span><br>
          {{count(Session::get('cart_item'))}} item(s)</span> </button>
                    <ul class="dropdown-menu pull-right cart-dropdown-menu">
                        <li>
                            <table class="table table-striped">
                                <tbody>
                                @foreach($items as $item)
                                <tr>
                                    <td class="text-center"><a href="#"><img class="img-thumbnail" title="lorem ippsum dolor dummy" alt="lorem ippsum dolor dummy" src="{{asset('image_upload/images/'.$item['product_image'])}}" style="width: 50px; height:59px"></a></td>
                                    <td class="text-left"><a href="#">{{$item['product_name']}}</a></td>
                                    <td class="text-right">x {{$item['quantity']}}</td>
                                    <td class="text-right">{{$item['price']}}</td>
                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <p class="text-right"> <span class="btn-viewcart"><a href="{{route('my_cart')}}"><strong><i class="fa fa-shopping-cart"></i> View Cart</strong></a></span> <span class="btn-checkout"><a href="{{route('check_out')}}"><strong><i class="fa fa-share"></i> Checkout</strong></a></span> </p>
                        </li>
                    </ul>
                </div>
            </div>
                @else
                <div class="col-sm-4 col-xs-12 header-right" id="header-cart">
                    <div id="cart" class="btn-group btn-block">
                        <button type="button" class="btn btn-inverse btn-block btn-lg dropdown-toggle cart-dropdown-button"> <span id="cart-total"><span class="cart-title">Shopping Cart</span><br>
         0 item(s)</span> </button>
                        <ul class="dropdown-menu pull-right cart-dropdown-menu">
                            <li>
                                <table class="table table-striped">
                                    <tbody>
                                   Chưa có sản phẩm
                                    </tbody>
                                </table>
                            </li>
                        </ul>
                    </div>
                </div>
                @endif
        </div>
    </div>
</header>
