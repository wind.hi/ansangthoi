<nav id="menu" class="navbar">
    <div class="nav-inner container">
        <div class="navbar-header"><span id="category" class="visible-xs">Categories</span>
            <button type="button" class="btn btn-navbar navbar-toggle" ><i class="fa fa-bars"></i></button>
        </div>
        <div class="navbar-collapse">
            <ul class="main-navigation">
                <li><a href="index.html"   class="parent"  >Home</a> </li>
                <li><a href="{{route('get_category_product','banh-mi')}}"   class="parent"  >Bánh Mì</a> </li>
                <li><a href="{{route('get_category_product','xoi')}}"   class="parent"  >Xôi</a> </li>
                <li><a href="{{route('get_category_product','do-an-vat')}}"   class="parent"  >Đồ ăn vặt</a> </li>
                <li><a href="{{route('get_category_product','thuc-uong')}}"   class="parent"  >Thức uống</a> </li>
                <li><a href="category.html"   class="parent"  >Blog</a> </li>
            </ul>
        </div>
    </div>
</nav>
