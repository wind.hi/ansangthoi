@extends('site.layout.site')

@section('title', 'home')
@section('banner')
    <div class="mainbanner">
        <div id="main-banner" class="owl-carousel home-slider">
            <div class="item"> <a href="#"><img src="{{asset('assets/image/banners/Main-Banner1.jpg')}}" alt="main-banner1" class="img-responsive" /></a> </div>
            <div class="item"> <a href="#"><img src="{{asset('assets/image/banners/Main-Banner2.jpg')}}" alt="main-banner2" class="img-responsive" /></a> </div>
            <div class="item"> <a href="#"><img src="{{asset('assets/image/banners/Main-Banner3.jpg')}}" alt="main-banner3" class="img-responsive" /></a> </div>
        </div>
    </div>
@append
{{--@section('bs')--}}
{{--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--}}
{{--    @append--}}
@section('content')
<div class="row">

    <div class="cms_banner ">
        <div class="col-md-4 cms-banner-left"> <a href="#"><img alt="#" src="image/banners/subbanner1.jpg"></a> </div>
        <div class="col-md-4 cms-banner-middle-top">
            <div class="md1"><a href="#"> <img alt="#" src="image/banners/subbanner2.jpg"></a> </div>
            <div class="md2"><a href="#"> <img alt="#" src="image/banners/subbanner2-1.jpg"></a></div>
        </div>
        <div class="col-md-4 cms-banner-right"> <a href="#"><img alt="#" src="image/banners/subbanner3.jpg"></a> </div>
    </div>
</div>
<div class="row">
    <div id="content" class="col-sm-12">
        <div class="customtab">
            <div id="tabs" class="customtab-wrapper">
                <ul class='customtab-inner'>
                    <li class='tab'><a href="#tab-latest">Mua nhiều nhất</a></li>

                </ul>
            </div>
            <div id="tab-latest" class="tab-content">
                <div class="box">
                    <div id="latest-slidertab" class="row owl-carousel product-slider">
                        @foreach($productBestSale as $product)
                        <div class="item">
                            <div class="product-thumb transition">
                                <div class="image product-imageblock"> <a href="{{route('get_detail_product',$product->slug)}}"><img src="{{asset('image_upload/images/'.$product->image)}}" alt="lorem ippsum dolor dummy" title="lorem ippsum dolor dummy" class="img-responsive" /> </a>
                                    <div class="button-group">
                                        <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                                        <button type="button" class="addtocart-btn" data-toggle="modal" data-target="#modalDetaiProduct" data-id="{{$product->product_id}}" onclick="return showProduct(this);">Thêm vào giỏ</button>
                                        <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                                    </div>
                                </div>
                                <div class="caption product-detail">
                                    <h4 class="product-name"><a href="{{route('get_detail_product',$product->slug)}}" title="lorem ippsum dolor dummy">{{$product->title}}  </a></h4>
                                    <p class="price product-price">{{number_format($product->price,'0',',','.')}} đ<span class="price-tax">Ex Tax: $100.00</span></p>
                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
                                </div>

                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <h3 class="productblock-title">Đang khuyến mãi</h3>
            <div class="box">
                <div id="Weekly-slider" class="row owl-carousel product-slider">
                    @foreach($productDiscount as $product)
                        <div class="item">
                            <div class="product-thumb transition">
                                <div class="image product-imageblock"> <a href="{{route('get_detail_product',$product->slug)}}"><img src="{{asset('image_upload/images/'.$product->image)}}" alt="lorem ippsum dolor dummy" title="lorem ippsum dolor dummy" class="img-responsive" /> </a>
                                    <div class="button-group">
                                        <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                                        <button type="button" class="addtocart-btn" data-toggle="modal" data-target="#modalDetaiProduct" data-id="{{$product->product_id}}" onclick="return showProduct(this);">Thêm vào giỏ</button>
                                        <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                                    </div>
                                </div>
                                <div class="caption product-detail">
                                    <h4 class="product-name"><a href="{{route('get_detail_product',$product->slug)}}" title="lorem ippsum dolor dummy">{{$product->title}} </a></h4>
                                    <p class="price product-price">{{number_format($product->discount,'0',',','.')}} đ <span class="price-old" style="color: #8B0000">{{number_format($product->price,'0',',','.')}} đ </span></p>
                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
                                </div>

                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
        <div class="parallax">
            <ul id="testimonial" class="row owl-carousel product-slider">
                <li class="item">
                    <div class="panel-default">
                        <div class="testimonial-desc">Rem ipsum doLorem ipsum ut Rem ipsum doLorem ipsum ut labore et dolore malabore et dolore maipsum doLorem ipsum ut labore et dolore magna.Lorem ipsum doLorem ipsum dolor sit amet, consectetur adipisicing</div>
                        <div class="testimonial-image"><img src="image/t1.jpg" alt="#"></div>
                        <div class="testimonial-name"><h2>Nunc rutrum scel potent</h2></div>
                        <div class="testimonial-designation"><p>Web Designer</p></div>

                    </div>
                </li>
                <li class="item">
                    <div class="panel-default">
                        <div class="testimonial-desc">Rem ipsum doLoremRem ipsum doLorem ipsum ut labore et dolore ma ipsum ut labore et dolore  Rem ipsum doLorem ipsum ut labore et dolore mamagna.Lorem ipsum doLorem ipsum dolor sit amet, consectetur adipisicing</div>
                        <div class="testimonial-image"><img src="image/t2.jpg" alt="#"></div>
                        <div class="testimonial-name"><h2>Nunc rutrum scel potent</h2></div>
                        <div class="testimonial-designation"><p>Web Deweloper</p></div>

                    </div>
                </li>
                <li class="item">
                    <div class="panel-default">
                        <div class="testimonial-desc">RemRem ipsum doLorem ipsum ut labore et dolore ma ipsum doLorem ipsum ut labore et dolore magna.Rem ipsum doLorem ipsum ut labore et dolore maLorem ipsum doLorem ipsum dolor sit amet, consectetur adipisicing</div>
                        <div class="testimonial-image"><img src="image/t3.jpg" alt="#"></div>
                        <div class="testimonial-name"><h2>Nunc rutrum scel potent</h2></div>
                        <div class="testimonial-designation"><p>Web Designer</p></div>

                    </div>
                </li>
                <li class="item">
                    <div class="panel-default">
                        <div class="testimonial-desc">Rem ipsum doLorem Rem ipsum doLorem ipsum ut labore et dolore maipsum ut  Rem ipsum doLorem ipsum ut labore et dolore ma labore et dolore magna.Lorem ipsum doLorem ipsum dolor sit amet, consectetur adipisicing</div>
                        <div class="testimonial-image"><img src="image/t4.jpg" alt="#"></div>
                        <div class="testimonial-name"><h2>Nunc rutrum scel potent</h2></div>
                        <div class="testimonial-designation"><p>Web Deweloper</p></div>

                    </div>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="cms_banner">
                <div class="col-md-4 cms-banner-left"> <a href="#"><img alt="#" src="image/banners/subbanner5.jpg"></a> </div>
                <div class="col-md-4 cms-banner-middle"> <a href="#"><img alt="#" src="image/banners/subbanner6.jpg"></a> </div>
                <div class="col-md-4 cms-banner-right"> <a href="#"><img alt="#" src="image/banners/subbanner7.jpg"></a> </div>
            </div>
        </div>
        <div id="subbanner4" class="banner" >
            <div class="item"> <a href="#"><img src="image/banners/subbanner4.jpg" alt="Sub Banner4" class="img-responsive" /></a> </div>
        </div>
        <h3 class="productblock-title">ComBo BreakFast - Mua nhanh, ăn lẹ</h3>
        <div class="box">
            <div id="feature-slider" class="row owl-carousel product-slider">
                @foreach($productCombo as $product)
                    <div class="item">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"> <a href="{{route('get_detail_product',$product->slug)}}"><img src="{{asset('image_upload/images/'.$product->image)}}" alt="lorem ippsum dolor dummy" title="lorem ippsum dolor dummy" class="img-responsive" /> </a>
                                <div class="button-group">
                                    <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                                    <button type="button" class="addtocart-btn" data-toggle="modal" data-target="#modalDetaiProduct" data-id="{{$product->product_id}}" onclick="return showProduct(this);">Thêm vào giỏ</button>
                                    <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name"><a href="{{route('get_detail_product',$product->slug)}}" title="lorem ippsum dolor dummy">{{$product->title}} </a></h4>
                                <p class="price product-price">{{number_format($product->price,'0',',','.')}} đ</p>
                                <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="blog">
            <div class="blog-heading">
                <h3>Latest Blogs</h3>
            </div>
            <div class="blog-inner box">
                <ul class="list-unstyled blog-wrapper" id="latest-blog">
                    <li class="item blog-slider-item">
                        <div class="panel-default">
                            <div class="blog-image"> <a href="#" class="blog-imagelink"><img src="image/blog/blog_1.jpg" alt="#"></a> <span class="blog-hover"></span> <span class="blog-date">06/07/2015</span> <span class="blog-readmore-outer"><a href="#" class="blog-readmore">Read More</a></span> </div>
                            <div class="blog-content"> <a href="#" class="blog-name">
                                    <h2>Nunc rutrum scel potent</h2>
                                </a>
                                <div class="blog-desc">Rem ipsum doLorem ipsum ut labore et dolore magna.Lorem ipsum doLorem ipsum dolor sit amet, consectetur adipisicing...</div>
                                <a href="#" class="blog-readmore">Read More</a> <span class="blog-date">06/07/2015</span> </div>
                        </div>
                    </li>
                    <li class="item blog-slider-item">
                        <div class="panel-default">
                            <div class="blog-image"> <a href="#" class="blog-imagelink"><img src="image/blog/blog_2.jpg" alt="#"></a> <span class="blog-hover"></span> <span class="blog-date">06/07/2015</span> <span class="blog-readmore-outer"><a href="#" class="blog-readmore">Read More</a></span> </div>
                            <div class="blog-content"> <a href="#" class="blog-name">
                                    <h2>Nunc rutrum scel potent</h2>
                                </a>
                                <div class="blog-desc">Rem ipsum doLorem ipsum ut labore et dolore magna.Lorem ipsum doLorem ipsum dolor sit amet, consectetur adipisicing...</div>
                                <a href="singale-blog.html" class="blog-readmore">Read More</a> <span class="blog-date">06/07/2015</span> </div>
                        </div>
                    </li>
                    <li class="item blog-slider-item">
                        <div class="panel-default">
                            <div class="blog-image"> <a href="#" class="blog-imagelink"><img src="image/blog/blog_3.jpg" alt="#"></a> <span class="blog-hover"></span> <span class="blog-date">06/07/2015</span> <span class="blog-readmore-outer"><a href="singale-blog.html" class="blog-readmore">Read More</a></span> </div>
                            <div class="blog-content"> <a href="#" class="blog-name">
                                    <h2>Nunc rutrum scel potent</h2>
                                </a>
                                <div class="blog-desc">Rem ipsum doLorem ipsum ut labore et dolore magna.Lorem ipsum doLorem ipsum dolor sit amet, consectetur adipisicing...</div>
                                <a href="singale-blog.html" class="blog-readmore">Read More</a> <span class="blog-date">06/07/2015</span> </div>
                        </div>
                    </li>
                    <li class="item blog-slider-item">
                        <div class="panel-default">
                            <div class="blog-image"> <a href="#" class="blog-imagelink"><img src="image/blog/blog_4.jpg" alt="#"></a> <span class="blog-hover"></span> <span class="blog-date">06/07/2015</span> <span class="blog-readmore-outer"><a href="#" class="blog-readmore">Read More</a></span> </div>
                            <div class="blog-content"> <a href="#" class="blog-name">
                                    <h2>Nunc rutrum scel potent</h2>
                                </a>
                                <div class="blog-desc">Rem ipsum doLorem ipsum ut labore et dolore magna.Lorem ipsum doLorem ipsum dolor sit amet, consectetur adipisicing...</div>
                                <a href="#" class="blog-readmore">Read More</a> <span class="blog-date">06/07/2015</span> </div>
                        </div>
                    </li>
                    <li class="item blog-slider-item">
                        <div class="panel-default">
                            <div class="blog-image"> <a href="#" class="blog-imagelink"><img src="image/blog/blog_5.jpg" alt="#"></a> <span class="blog-hover"></span> <span class="blog-date">06/07/2015</span> <span class="blog-readmore-outer"><a href="#" class="blog-readmore">Read More</a></span> </div>
                            <div class="blog-content"> <a href="#" class="blog-name">
                                    <h2>Nunc rutrum scel potent</h2>
                                </a>
                                <div class="blog-desc">Rem ipsum doLorem ipsum ut labore et dolore magna.Lorem ipsum doLorem ipsum dolor sit amet, consectetur adipisicing...</div>
                                <a href="#" class="blog-readmore">Read More</a> <span class="blog-date">06/07/2015</span> </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div id="brand_carouse" class="owl-carousel brand-logo">
            <div class="item text-center"> <a href="#"><img src="image/brand/brand1.png" alt="Disney" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="image/brand/brand2.png" alt="Dell" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="image/brand/brand3.png" alt="Harley" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="image/brand/brand4.png" alt="Canon" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="image/brand/brand5.png" alt="Canon" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="image/brand/brand6.png" alt="Canon" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="image/brand/brand7.png" alt="Canon" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="image/brand/brand8.png" alt="Canon" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="image/brand/brand9.png" alt="Canon" class="img-responsive" /></a> </div>
            <div class="item text-center"> <a href="#"><img src="image/brand/brand5.png" alt="Canon" class="img-responsive" /></a> </div>
        </div>
    </div>
</div>
{{--modal quick view--}}
<div class="modal fade" id="modalDetaiProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h3 class="modal-title">HTML5 is a markup language</h3>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body" id="modal-content">


            </div>

        </div>
    </div>
</div>
{{--    end modal--}}
{{--    modal notifi add cart--}}
<div class="modal fade" id="modalNotifi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px;">
        <div class="modal-content">
            <div class="modal-body" id="modal-content">
                <h4>Đã thêm vào giỏ hàng</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tiếp tục mua</button>
                <a href="{{route('my_cart')}}" target="_self"><button type="button" class="btn btn-primary">Đi tới giỏ hàng</button></a>
            </div>
        </div>
    </div>
</div>
{{--    end modal--}}
    @endsection
@push('scripts')
   <script type="text/javascript">
       function showProduct(e){
           var product_id = $(e).data('id');
            $.ajax({
                url:'{{route('detail_product')}}',
                method:'GET',
                data:{
                    product_id:product_id
                },
                    success: function (data) {
                        $('#modal-content').html(data);
                        // $("#notifiSuccess").modal('show');

                    },
                    error: function(error) {
                    }
            },
            );
       }
   </script>
   <script type="text/javascript">
       function addToCart(e){
               var product_id = $("input[name='product_id_modal']").val();
               var quantity = $("input[name='quantity']").val();
           $.ajax({
                   url:'{{route('add_to_cart')}}',
                   method:'POST',
                   data:{
                       _token: "{{ csrf_token() }}",
                       product_id:product_id,
                       quantity:quantity
                   },
                   success: function (data) {
                       console.log(data)
                       $("#modalNotifi").modal('show');
                       $("#modalDetaiProduct").modal('hide');
                       $.ajax({
                           url:'{{route('update_cart')}}',
                           method:'GET',
                           success: function (data) {
                               $('#header-cart').empty();
                               $('#header-cart').html(data);
                               console.log(data);
                           },
                           error: function(error) {
                           }
                       })
                   },
                   error: function(error) {
                   }
               },
           );
       }


   </script>
    @endpush
