@extends('site.layout.site')

@section('title', 'my cart')
@section('content')
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
        <li><a href="cart.html">Shopping Cart</a></li>
    </ul>
    <div class="row">
        <div id="column-left" class="col-sm-3 hidden-xs column-left">
            <div class="column-block">
                <div class="column-block">
                    <div class="columnblock-title">Categories</div>
                    <div class="category_block">
                        <ul class="box-category treeview-list treeview collapsable">
                            <li class="expandable"><div class="hitarea expandable-hitarea"></div><a href="#" class="activSub">Desktops</a>
                                <ul class="collapsable" style="display: none;">
                                    <li><a href="#">PC</a></li>
                                    <li class="last"><a href="#">MAC</a></li>
                                </ul>
                            </li>
                            <li class="expandable"><div class="hitarea expandable-hitarea"></div><a href="#" class="activSub">Laptops &amp; Notebooks</a>
                                <ul class="collapsable" style="display: none;">
                                    <li><a href="#">Macs</a></li>
                                    <li class="last"><a href="#">Windows</a></li>
                                </ul>
                            </li>
                            <li class="expandable"><div class="hitarea expandable-hitarea"></div><a href="#" class="activSub">Components</a>
                                <ul class="collapsable" style="display: none;">
                                    <li><a href="#">Mice and Trackballs</a></li>
                                    <li class="expandable"><div class="hitarea expandable-hitarea"></div><a href="#" class="activSub">Monitors</a>
                                        <ul class="collapsable" style="display: none;">
                                            <li><a href="#">test 1</a></li>
                                            <li class="last"><a href="#">test 2</a></li>
                                        </ul>
                                    </li>
                                    <li class="last"><a href="#">Windows</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Tablets</a></li>
                            <li><a href="#">Software</a></li>
                            <li><a href="#">Phones &amp; PDAs</a></li>
                            <li><a href="#">Cameras</a></li>
                            <li class="last"><a href="#">MP3 Players</a></li>
                        </ul>
                    </div>
                </div>
                <h3 class="productblock-title">Bestsellers</h3>
                <div class="row bestseller-grid product-grid">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"> <a href="#"> <img src="image/product/2product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"> </a>
                                <div class="button-group">
                                    <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                    <button type="button" class="addtocart-btn">Add to Cart</button>
                                    <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name"> <a href="product.html" title="women's clothing">Clothing</a> </h4>
                                <p class="price product-price"> <span class="price-new">$254.00</span><span class="price-tax">Ex Tax: $210.00</span> </p>
                            </div>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"> <a href="#"> <img src="image/product/3product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"> </a>
                                <div class="button-group">
                                    <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                    <button type="button" class="addtocart-btn">Add to Cart</button>
                                    <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name"> <a href="product.html" title="women's clothing">Clothing</a> </h4>
                                <p class="price product-price"> <span class="price-new">$254.00</span><span class="price-tax">Ex Tax: $210.00</span> </p>
                            </div>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item last">
                        <div class="product-thumb transition">
                            <div class="image product-imageblock"> <a href="#"> <img src="image/product/4product50x59.jpg" alt="women's clothing" title="women's clothing" class="img-responsive"> </a>
                                <div class="button-group">
                                    <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                    <button type="button" class="addtocart-btn">Add to Cart</button>
                                    <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name"> <a href="product.html" title="women's clothing">Clothing</a> </h4>
                                <p class="price product-price"> <span class="price-new">$254.00</span><span class="price-tax">Ex Tax: $210.00</span> </p>
                            </div>
                            <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9" id="content">
            @if (session('cart_item'))
            <h1>Shopping Cart                &nbsp;(10.00kg) </h1>
            <form enctype="multipart/form-data" method="post" action="#">
                <div class="table-responsive">
                    <table class="table table-bordered" id="tableItem">
                        <thead>
                        <tr>
                            <td class="text-center"></td>
                            <td class="text-left">Sản phẩm</td>
                            <td class="text-left">Danh mục</td>
                            <td class="text-left">Số lượng</td>
                            <td class="text-right">Đơn giá</td>
                            <td class="text-right">Thành tiền</td>
                        </tr>
                        </thead>
                        <tbody>
                        @if (session('cart_item'))
                        <?php
                            $total = 0;
                        ?>
                       @foreach(Session::get('cart_item') as $item)
                        <tr>
                            <td class="text-center"><a href="product.html"><img class="img-thumbnail" title="{{$item['product_name']}}" alt="{{$item['product_name']}}" src="{{asset('image_upload/images/'.$item['product_image'])}}" style="width: 72px; height: auto;"></a></td>
                            <td class="text-left"><a href="product.html">{{$item['product_name']}}</a></td>
                            <td class="text-left">{{$item['product_category']}}</td>
                            <td class="text-left"><div style="max-width: 200px;" class="input-group btn-block">
                                    <input type="text" class="form-control quantity" size="1" value="{{$item['quantity']}}" name="quantity">
                                    <input type="hidden"  class="hidden_quantity"  value="{{$item['id']}}" >
                                    <span class="input-group-btn">
                    <button class="btn btn-primary" title="" data-toggle="tooltip" type="button" data-original-title="Update" data-id="{{$item['id']}}" onclick="return updateItem(this)"><i class="fa fa-refresh"></i></button>
                    <button class="btn btn-danger" title="" data-toggle="tooltip" type="button" data-original-title="Remove" data-id="{{$item['id']}}" onclick="return removeItem(this)"><i class="fa fa-times-circle"></i></button>
                    </span></div></td>
                            <td class="text-right">{{number_format($item['price'],0,',','.')}} vnđ</td>
                            <td class="text-right">{{number_format($item['price'] * $item['quantity'],0,',','.')}} vnđ</td>
{{--                            {{$total += $item['price'] * $item['quantity'] }}--}}
                            @php
                                $total += $item['price'] * $item['quantity']
                            @endphp
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </form>

            <br>

            <div class="row">
                <div class="col-sm-4 col-sm-offset-8">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td class="text-right"><strong>Sub-Total:</strong></td>
                            <td class="text-right">$210.00</td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Eco Tax (-2.00):</strong></td>
                            <td class="text-right">$2.00</td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>VAT (20%):</strong></td>
                            <td class="text-right"></td>
                        </tr>
                        <tr id="total-price">
                            <td class="text-right"><strong>Total:</strong></td>
                            <td class="text-right">{{number_format($total,0,',','.')}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="buttons">
                <div class="pull-left"><a class="btn btn-default" href="index.html">Continue Shopping</a></div>
                <div class="pull-right"><a class="btn btn-primary" href="checkout.html">Checkout</a></div>
            </div>

            @else
            <p>Bạn chưa có sản phẩm nào</p>
                @endif
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        function removeItem(e) {
            var product_id = $(e).data('id');
            console.log(product_id);
            $.ajax({
                    url:'{{route('remove_item_cart')}}',
                    method:'GET',
                    data:{
                        product_id:product_id
                    },
                    success: function (data) {
                        // $("#notifiSuccess").modal('show');
                        if(data == 0){
                            $("#content").empty();
                            $("#content").append('<p>Bạn chưa có sản phẩm gì hết à</p>')
                        }
                        $("#tableItem tbody").empty();
                        $(".tooltip").empty();
                        $("#tableItem tbody").html(data);
                        // location.reload();
                        $.ajax({
                            url:'{{route('get_total_price')}}',
                            method:'GET',
                            success: function(data){
                                $("#total-price").find('td').remove();
                                $("#total-price").append('<td class="text-right"><strong>Total:</strong></td>');

                                $("#total-price").append(' <td class="text-right">'+data+'</td>');
                            },
                            error: function(){
                                alert("Error");
                            }
                        });
                    },
                    error: function(error) {
                    }
                },
            );
        }
    </script>
    <script>
        function updateItem(e) {
            var product_id = $(e).data('id');
            var hidden_quantity = $(".hidden_quantity");
            var quantity = $("input[name='quantity']"); //ko lấy ra đc Size
            var num = '';
            for (var i = 0; i < hidden_quantity.length; i++)
            {
                for (var i = 0; i < quantity.length; i++)
                {
                    if(product_id == hidden_quantity[i].value)
                    {
                        num = quantity[i].value;
                    }
                }
            }
            // var quantity = $("input[name='quantity']").val();
            // var quantity = $("input[name='quantity']"); //ko lấy ra đc Size
            // var num = '';
            // for (var i = 0; i < quantity.length; i++)
            // {
            //     if(product_id == hidden_quantity)
            //     {
            //         num = quantity[i].value;
            //     }
            // }
            console.log(product_id);
            // console.log(hidden_quantity);
            console.log(num);
            $.ajax({
                    url:'{{route('update_item_cart')}}',
                    method:'GET',
                    data:{
                        product_id:product_id,
                        quantity:num,
                    },
                    success: function (data) {
                        // $("#notifiSuccess").modal('show');
                        $("#tableItem tbody").empty();

                        $(".tooltip").empty();
                        $("#tableItem tbody").html(data);
                        // $("#total-price").find('td').remove();
                        // // $("#total-price").append('<td class="text-right"><strong>Total:</strong></td>');
                        //
                        // $("#total-price").append(' <td class="text-right">'+$total.toFixed(2)+'</td>');

                        // location.reload();
                        $.ajax({
                            url:'{{route('get_total_price')}}',
                            method:'GET',
                            success: function(data){
                                $("#total-price").find('td').remove();
                                $("#total-price").append('<td class="text-right"><strong>Total:</strong></td>');

                                $("#total-price").append(' <td class="text-right">'+data+'</td>');
                            },
                            error: function(){
                                alert("Error");
                            }
                        });
                    },
                    error: function(error) {
                    }
                },
            );
        }
    </script>
    @endpush
