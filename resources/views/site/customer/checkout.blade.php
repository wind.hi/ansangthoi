@extends('site.layout.site')

@section('title', 'check out')
@section('content')
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
        <li><a href="cart.html">Shopping Cart</a></li>
    </ul>
    @if (session('cart_item'))
        <div class="row cart-head">
        <div class="container">
            <div class="row">
                <p></p>
            </div>
            <div class="row">
                <div style="display: table; margin: auto;">
                    <span class="step step_complete"> <a href="#" class="check-bc">Cart</a> <span class="step_line step_complete"> </span> <span class="step_line backline"> </span> </span>
                    <span class="step step_complete"> <a href="#" class="check-bc">Checkout</a> <span class="step_line "> </span> <span class="step_line step_complete"> </span> </span>
                    <span class="step_thankyou check-bc step_complete">Thank you</span>
                </div>
            </div>
            <div class="row">
                <p></p>
            </div>
        </div>
    </div>
    <div class="row cart-body">
        <form class="form-horizontal" method="post" action="{{route('create_order')}}">
            {!! csrf_field() !!}
            {{ method_field('POST') }}
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-push-6 col-sm-push-6">
                <!--REVIEW ORDER-->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Review Order <div class="pull-right"><small><a class="afix-1" href="#">Edit Cart</a></small></div>
                    </div>
                    @php
                    $total = 0;
                    @endphp
                    <div class="panel-body">
                        @foreach(Session::get('cart_item') as $item)

                        <div class="form-group">
                            <div class="col-sm-3 col-xs-3">
                                <img class="img-responsive" src="{{asset('image_upload/images/'.$item['product_image'])}}" />
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="col-xs-12">{{$item['product_name']}}</div>
                                <div class="col-xs-12"><small>Quantity:<span>{{$item['quantity']}}</span></small></div>
                            </div>
                            <div class="col-sm-3 col-xs-3 text-right">
                                <h6>{{number_format($item['price'],'0',',','.')}} vnđ</h6>
                            </div>
                        </div>
                        <div class="form-group"><hr /></div>
                            @php
                                $total += $item['price'] * $item['quantity']
                            @endphp
                        @endforeach
                        <div class="form-group">
                            <div class="col-xs-12">
                                <strong>Tổng tiền</strong>
                                <div class="pull-right"><span>{{number_format($total,0,',','.')}} vnđ</span></div>
                            </div>
                        </div>
                            @if(\Illuminate\Support\Facades\Auth::check())
                            <div class="col-xs-12">
                                <small>Giảm giá thành viên</small>
                                <div class="pull-right"><span>-30percent</span></div>
                            </div>
                        <div class="form-group"><hr /></div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <strong>Order Total</strong>
                                <div class="pull-right"><span>{{number_format($total - ($total * (30/100)),0,',','.')}} vnđ</span></div>
                                <input type='hidden' name="total_price" value="{{$total - ($total * (30/100))}}" />

                            </div>
                        </div>
                        @else
                                <div class="col-xs-12">
                                    <small>Giảm giá thành viên</small>
                                    <div class="pull-right"><span>-0percent</span></div>
                                </div>
                                <div class="form-group"><hr /></div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <strong>Order Total</strong>
                                        <div class="pull-right"><span>{{number_format($total,0,',','.')}} vnđ</span></div>
                                        <input type='hidden' name="total_price" value="{{$total}}" />

                                    </div>
                                </div>
                            @endif
                    </div>
                </div>
                <!--REVIEW ORDER END-->
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-pull-6 col-sm-pull-6">
                <!--SHIPPING METHOD-->
                <div class="panel panel-info">
                    <div class="panel-heading">Address</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <h4>Shipping Address</h4>
                            </div>
                        </div>
                        @if(\Illuminate\Support\Facades\Auth::check())
{{--                            <div class="col-md-12">--}}
{{--                                <select id="chooseDelivery" name="delivery" class="form-control">--}}
{{--                                    <option value="0">Sử dụng địa chỉ tài khoản</option>--}}
{{--                                    <option value="1">Dùng địa chỉ mới</option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="radio">
                                    <label>
                                        <input type="radio" checked="checked" value="0" name="chooseDelivery">
                                        Sử dụng thông tin tài khoản</label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="1" name="chooseDelivery">
                                        Nhập thông tin mới</label>
                                </div>
                            </div>

                        </div>

                        @endif

                        <div class="form-group">
                            <div class="col-md-12"><strong>Họ tên</strong></div>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="customer_name" value="" />
                            </div>
                        </div>
                        @if ($errors->has('customer_name'))
                            <span class="help-block">
                                         <strong>{{ $errors->first('customer_name') }}</strong>
                                        </span>
                        @endif
                        <div class="form-group">
                            <div class="col-md-12"><strong>Hình thức nhận</strong></div>
                            <div class="col-md-12">
                                <select id="CreditCardType" name="receive" class="form-control">
                                    <option value="" selected>--Chọn hình thức nhận--</option>
                                    <option value="ship">Ship tận nơi luôn</option>
                                    <option value="atshop">Lấy tại cửa hàng</option>
                                </select>
                            </div>
                        </div>
                        @if ($errors->has('receive'))
                            <span class="help-block">
                                         <strong>{{ $errors->first('receive') }}</strong>
                                        </span>
                        @endif
                        <div class="form-group choosestore">
                            <div class="col-md-12"><strong>Chọn cửa hàng</strong></div>
                            <div class="col-md-12">

                                <select id="CreditCardType-store" name="store" class="form-control">
                                    <option value="" selected>--Chọn cửa hàng gần bạn--</option>
                                    @foreach($stores as $store)
                                        <option value="{{$store->store_id}}">{{$store->store_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
{{--                        @if ($errors->has('store'))--}}
{{--                            <span class="help-block">--}}
{{--                                         <strong>{{ $errors->first('store') }}</strong>--}}
{{--                                        </span>--}}
{{--                        @endif--}}
                        <div class="form-group address">
                            <div class="col-md-12"><strong>Địa chỉ nhận hàng</strong></div>
                            <div class="col-md-12">
                                <input type="text" name="address" class="form-control" value="" />
                            </div>
                        </div>
                        @if ($errors->has('address'))
                            <span class="help-block">
                                         <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                        @endif
                        <div class="form-group">
                            <div class="col-md-12"><strong>Số điện thoại</strong></div>
                            <div class="col-md-12">
                                <input type="text" name="phone" class="form-control" value="" />
                            </div>
                        </div>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                         <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                        @endif
                        <div class="form-group">
                            <div class="col-md-12"><strong>Ngày nhận hàng</strong></div>
                            <div class="col-md-12 " id="datepicker"  data-date-format="dd-mm-yyyy">
                                <input type="date" class="form-control" type="text" id="date_order" name="date_order">
                            </div>
                        </div>
                        @if ($errors->has('date_order'))
                            <span class="help-block">
                                         <strong>{{ $errors->first('date_order') }}</strong>
                                        </span>
                        @endif
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button type="submit" class="btn btn-primary btn-submit-fix">Place Order</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--SHIPPING METHOD END-->
                <!--CREDIT CART PAYMENT-->

                <!--CREDIT CART PAYMENT END-->
            </div>

        </form>
    </div>
    @endif
@endsection
@push('scripts')

    <script type="text/javascript">
        $('.choosestore').hide();
        $('#CreditCardType').change(function () {
            var value = $('#CreditCardType option:selected').val();
            if(value == '0')
            {
                $('.choosestore').hide();
                $('.address').hide();
            }
            if(value == 'atshop')
            {
                $('.choosestore').show();
                $('.address').hide();
                $('input[name="address"]').val('lấy tại cửa hàng');

            }
            if(value == 'ship')
            {
                $('.choosestore').hide();
                // $('#CreditCardType-store option').val('-1');
                $('.address').show();
                <?php if(\Illuminate\Support\Facades\Auth::check()){ ?>
                <?php $customer = \App\Entity\Customer::where('customer_user_id',Auth::id())->first() ?>
                $('input[name="address"]').val('{{$customer->address}}');
                <?php }else{?>
                $('input[name="address"]').val('');
                <?php }?>
            }

        });
    </script>
    <script>
        $(document).ready(function () {
            <?php if(\Illuminate\Support\Facades\Auth::check()){ ?>
                <?php $customer = \App\Entity\Customer::where('customer_user_id',Auth::id())->first() ?>
                $('input[name="customer_name"]').val('{{\Illuminate\Support\Facades\Auth::user()->name}}');
                $('input[name="address"]').val('{{$customer->address}}');
            <?php } else{?>
            $('input[name="customer_name"]').val('');
            $('input[name="address"]').val('');


            <?php }?>
            $('input[type=radio][name=chooseDelivery]').change(function() {
                    <?php if(\Illuminate\Support\Facades\Auth::check()){ ?>

                if (this.value == '0') {
                    $('input[name="customer_name"]').val('{{\Illuminate\Support\Facades\Auth::user()->name}}');
                    $('input[name="address"]').val('{{$customer->address}}');

                    }
                else if (this.value == '1') {
                    $('input[name="customer_name"]').val('');
                    $('input[name="address"]').val('');
                }
                <?php }?>
            });
            {{--$('#chooseDelivery').change(function () {--}}
            {{--    <?php if(\Illuminate\Support\Facades\Auth::check()){ ?>--}}
            {{--    var value = $('#chooseDelivery option:selected').val();--}}
            {{--    if(value == '0')--}}
            {{--    {--}}
            {{--        $('input[name="customer_name"]').val('{{\Illuminate\Support\Facades\Auth::user()->name}}');--}}

            {{--    }--}}
            {{--    if(value == '1')--}}
            {{--    {--}}

            {{--        $('input[name="customer_name"]').val('');--}}

            {{--    }--}}
            {{--        <?php }?>--}}

            {{--});--}}
        })
    </script>
@endpush
