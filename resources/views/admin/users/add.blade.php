@extends('admin.layout.admin')

@section('title', 'Thêm tài khoản admin')

@section('content')

    <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Thêm tài khoản</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="{{ route('add_user') }}" method="POST">
                    {!! csrf_field() !!}
                    {{ method_field('POST') }}
                    <div class="card-body">
                        @if (session('status'))
                            <ul>
                                <li class="text-danger"> {{ session('status') }}</li>
                            </ul>
                        @endif
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="nhập tên danh mục" name="email">
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                         <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                        @endif
                        <div class="form-group">
                            <label for="exampleInputEmail1">Password</label>
                            <input type="password" class="form-control" id="exampleInputEmail1" placeholder="nhập tên danh mục" name="password">
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                         <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                        @endif
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="nhập tên danh mục" name="name">
                        </div>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                         <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                        @endif
                        <div class="form-group">
                            <label for="exampleInputEmail1">Phone</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="nhập tên danh mục" name="phone">
                        </div>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                         <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                        @endif
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>


        </div>
        <div class="col-md-3">

        </div>




    </div>
@endsection
