@extends('admin.layout.admin')

@section('title', 'Thêm mới sản phẩm')

@section('content')

    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Thêm sản phẩm</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="{{route('product.store')}}" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    {{ method_field('POST') }}
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên sản phẩm</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="nhập tên sản phẩm" name="title">
                        </div>
                        @if ($errors->has('title'))
                            <span class="help-block">
                                         <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                        @endif
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <!-- select -->
                                    <div class="form-group">
                                        <label>Danh mục</label>
                                        <select class="form-control select-parent" name="parent_category">
                                            <option value="" selected>-------chọn danh mục--------</option>
                                        @foreach($categories as $cate)
                                            <option value="{{$cate->category_id}}" title="{{$cate->title}}">{{$cate->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('parent_category'))
                                        <span class="help-block">
                                         <strong>{{ $errors->first('parent_category') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Danh mục con</label>
                                        <select class="form-control select-child" name="sub_category">
                                            <option value="" selected>-------chọn danh mục--------</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <p class="note-cate">Danh mục: </p>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Nguyên liệu</label>
                            <input type="text" class="form-control" name="material" placeholder="Nguyên liệu" id="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mã sản phẩm</label>
                            <input type="text" class="form-control" name="code" placeholder="Mã sản phẩm" id="">
                        </div>
                        @if ($errors->has('code'))
                            <span class="help-block">
                                         <strong>{{ $errors->first('code') }}</strong>
                                        </span>
                        @endif
                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá</label>
                            <input type="text" class="form-control formatPrice" name="price" placeholder="Giá sản phẩm" min="1" id="stepProductCreate3">
                        </div>
                        @if ($errors->has('price'))
                            <span class="help-block">
                                         <strong>{{ $errors->first('price') }}</strong>
                                        </span>
                        @endif
                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá khuyến mãi</label>
                            <input type="text" class="form-control formatPrice" name="discount" placeholder="Giá khuyến mãi" min="1" step="any" id="stepProductCreate4">
                        </div>

                        <div class="form-group date-sale">
                            <label for="exampleInputEmail1">Ngày bắt đầu khuyến mãi</label>
                            <div id="datepicker" class="input-group date" data-date-format="dd-mm-yyyy">
                                <input type="date" class="form-control" type="text" id="date_order" name="discount_start">
                            </div>
                        </div>
                        <div class="form-group date-sale">
                            <label for="exampleInputEmail1">Ngày kết thúc khuyến mãi</label>
                            <div id="datepicker" class="input-group date" data-date-format="dd-mm-yyyy" >
                                <input type="date" class="form-control" type="text" id="date_order" name="discount_end">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Mô tả</label>
                            <textarea name="description" class="form-control " id="editor1"></textarea>
                        </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Ảnh đại diện sản phẩm</label>
                                <div class="input-group" style="">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image">

                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        @if ($errors->has('image'))
                            <span class="help-block">
                                         <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                        @endif

                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>


        </div>
        <div class="col-md-2">

        </div>



    </div>
@endsection
@push('scripts')
    <script src="{{asset('adminstration/plugin/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.formatPrice').priceFormat({
                prefix: '',
                centsLimit: 0,
                thousandsSeparator: '.'
            });
            $(".note-cate").hide();
            $(".date-sale").hide();
            $("#stepProductCreate4").keyup(function(){
                $(".date-sale").show();
                if( $("#stepProductCreate4").val() == 0){
                    $(".date-sale").hide();
                }
            });
                bsCustomFileInput.init();

        });
    </script>
    <script>
        $('.select-parent').on('change', function (e) {
            $(".note-cate").show();
            var value = $('.select-parent option:selected').val();
            var title = $('.select-parent option:selected').attr('title');
            $(".note-cate").text('Danh mục: '+title+' ');
            $.ajax({
                url: '{{route('get_child_cate')}}',
                method: 'GET',
                data: {
                    cateID : value,
                },
                success: function (data) {
                    console.log(data);
                    var $subCate = $('.select-child').empty();

                    $subCate.append('<option value="" selected>-------chọn danh mục--------</option>');

                    for (i in data){
                        $subCate.append('<option value = ' + data[i].category_id + '>' + data[i].title + '</option>');
                }
                },
                error: function(error) {
                    console.log(error);
                }

            });
        });
        $('.select-child').change(function (e) {
            var title = $( ".select-child option:selected" ).text();
             $('.note-cate').find('span').remove();
             $('.note-cate').find('i').remove();
            $(".note-cate").append('<i class="fas fa-angle-right"></i><span> '+title+'</span>');
            if(title === '-------chọn danh mục--------'){
                $(".note-cate").find('span').remove();
            }
        });
    </script>
    @endpush
