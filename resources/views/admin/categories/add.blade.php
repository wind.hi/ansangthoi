@extends('admin.layout.admin')

@section('title', 'Thêm danh mục sản phẩm')

@section('content')

<div class="row">
    <div class="col-md-3">

    </div>
    <div class="col-md-6">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Thêm danh mục</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('category.store') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tên danh mục</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="nhập tên danh mục" name="title">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Danh mục cha</label>
                        <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="parent">
                            <option selected="selected" data-select2-id="3" value="0">------------------</option>
                            @foreach($categories as $cate)
                                {{print_r($cate)}}
                            <option value="{{$cate->category_id}}">{{$cate->title}}</option>

                            @endforeach

                        </select>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>


    </div>
    <div class="col-md-3">

    </div>




</div>
@endsection
