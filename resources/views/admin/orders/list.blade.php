@extends('admin.layout.admin')

@section('title', 'Danh sách đơn hàng')

@section('content')

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tables</h1>
    <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="order" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Mã đơn hàng</th>
                        <th>Loại đơn</th>
                        <th>Khách hàng</th>
                        <th>Địa chỉ nhận</th>
                        <th>SĐT</th>
                        <th>Ngày nhận</th>
                        <th>Tổng tiền</th>
                        <th>Thao tác</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection
@push('scripts')

    <script type="text/javascript">
        $(function() {
            $('#order').DataTable({
                pageLength: 100,
                type: 'GET',
                ajax: '{{route('dt_order')}}',
                columns: [
                    { data: 'order_id', name: 'order_id' },
                    { data: 'order_type', name: 'order_type' },
                    { data: 'customer_name', name: 'customer_name' },
                    { data: 'address_shipping', name: 'address_shipping' },
                    { data: 'phone', name: 'phone' },
                    { data: 'date_order', name: 'date_order' },
                    { data: 'total_price', name: 'total_price' },
                    { data: 'action', name: 'action', searchable: false, orderable: false }

                ],
                order : [
                    [ 0, 'desc' ]
                ]
            });
        });
    </script>


@endpush
