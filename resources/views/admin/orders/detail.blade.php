@extends('admin.layout.admin')

@section('title', 'Chi tiết đơn hàng')

@section('content')

    <div class="card card-default">
        <div class="card-header">
            <h4 class="card-title" style="font-size: 18px">Thông tin Order</h4>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Khách hàng</label>
                        <input type="text" class="form-control" value="{{$order->customer_name}}">
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <label>SĐT</label>
                        <input type="text" class="form-control" value="{{$order->phone}}" >

                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Địa chỉ nhận</label>
                        <input type="text" class="form-control" value="{{$order->address_shipping}}" >

                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <label>Ngày nhận</label>
                        <input type="text" class="form-control" value="{{$order->date_order}}"  >

                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label>Tổng tiền</label>
                        <input type="text" class="form-control" value="{{$order->total_price}}" >

                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <style type="text/css">
                    a:hover {
                        cursor:pointer;
                    }
                </style>
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label>Trạng thái</label>
                        @if($order->status == 0)
                        <div id="status-order">
                            <a href="#" class="btn btn-secondary btn-icon-split" onclick="return updateStatus({{$order->order_id}},1)" >
                                <span class="text">Đang chờ xác nhận</span>
                            </a>
                        </div>
                        @elseif($order->status == 1)
                            <div id="status-order">
                                <a href="#" class="btn btn-info btn-icon-split" onclick="return updateStatus({{$order->order_id}},2)" >
                                    <span class="text">Đã xác nhận</span>
                                </a> &nbsp;
                                <a onclick="return updateStatus({{$order->order_id}},0)" ><span class="text">về ban đầu</span></a>

                            </div>

                        @elseif($order->status == 2)
                            <div id="status-order">
                                <a href="#" class="btn btn-success btn-icon-split" onclick="return updateStatus({{$order->order_id}},3)" >
                                    <span class="text">Đã giao hàng</span>
                                </a> &nbsp;
                                <a onclick="return updateStatus({{$order->order_id}},0)" ><span class="text">về ban đầu</span></a>

                            </div>
                        @elseif($order->status == 3)
                            <div id="status-order">
                                <a href="#" class="btn btn-danger btn-icon-split" onclick="" >
                                    <span class="text">Đã hủy đơn</span>
                                </a> &nbsp;
                                <a onclick="return updateStatus({{$order->order_id}},0)" ><span class="text">về ban đầu</span></a>

                            </div>
                            @endif
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.card-body -->

    </div>
    <hr/>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="items" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Mã order</th>
                        <th>Sản phẩm</th>
                        <th>Số lương</th>
                        <th>Đơn giá</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>@endsection
@push('scripts')
    <script>
        function updateStatus(orderId,status) {
            if(status == 0){
                $.ajax({
                    url:'{{route('update_status_order')}}',
                    method:'GET',
                    data:{
                        order_id:orderId,
                        status:status
                    },
                    success: function(data){

                        $("#status-order").empty();
                        $("#status-order").html(data);
                    },
                    error: function(){
                        alert("Error");
                    }
                });
            }
            if(status == 1){
                $.ajax({
                    url:'{{route('update_status_order')}}',
                    method:'GET',
                    data:{
                        order_id:orderId,
                        status:status
                    },
                    success: function(data){
                        // $("#status-order").find('a').remove();
                        $("#status-order").empty();
                        $("#status-order").html(data);
                        // $("#status-order").find('span').remove();

                        // $("#status-order").append(data);

                    },
                    error: function(){
                        alert("Error");
                    }
                });
            }
            if(status == 2){
                $.ajax({
                    url:'{{route('update_status_order')}}',
                    method:'GET',
                    data:{
                        order_id:orderId,
                        status:status
                    },
                    success: function(data){

                        $("#status-order").empty();
                        $("#status-order").html(data);
                    },
                    error: function(){
                        alert("Error");
                    }
                });
            }
            if(status == 3){
                $.ajax({
                    url:'{{route('update_status_order')}}',
                    method:'GET',
                    data:{
                        order_id:orderId,
                        status:status
                    },
                    success: function(data){
                        $("#status-order").empty();
                        $("#status-order").html(data);
                    },
                    error: function(){
                        alert("Error");
                    }
                });
            }

        }
    </script>
    <script type="text/javascript">
        $(function() {
            $('#items').DataTable({
                pageLength: 100,
                type: 'GET',
                ajax: '{{route('dt_order_item',$order->order_id)}}',
                columns: [
                    { data: 'order_id', name: 'order_id' },
                    { data: 'product_name', name: 'product_name' },
                    { data: 'quantity', name: 'quantity' },
                    { data: 'price', name: 'price' },
                ],
                order : [
                    [ 0, 'desc' ]
                ]
            });
        });
    </script>

@endpush
