<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::group(['prefix'=>'admin', 'namespace'=>'Admin' ],function(){
    // login admin
    Route::get('login','LoginController@showLoginForm')->name('login_admin');
    Route::post('login','LoginController@login')->name('login_admin');
    Route::get('logout', 'LoginController@logOut')->name('logout');
});
Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware' => ['admin']],function(){
    Route::get('/', 'AdminController@home')->name('admin_home');
    // product category
    Route::get('get-child-cate', 'ProductController@getChildCategory')->name('get_child_cate');
    Route::get('create-product', 'ProductController@createProduct')->name('create_product');
    Route::resource('category','CategoryController');
    Route::resource('product','ProductController');
    // create user
    Route::get('create-user','UserController@showCreateUser')->name('create_user');
    Route::post('create-user','UserController@createUser')->name('add_user');
    // orders
    Route::resource('order','OrderController');
    Route::get('list-order','OrderController@anyDatatable')->name('dt_order');
    Route::get('update-status-order','OrderController@updateStatus')->name('update_status_order');
    Route::get('list-items/{id}','OrderController@anyDatatableOrderItem')->name('dt_order_item');
//    Route::get('detail-order','OrderController@detailOrder')->name('dt_order');



});

Route::group(['namespace'=>'Site' ],function(){
    // login site
    Route::get('login-customer','LoginController@showLoginForm')->name('login_customer');
    Route::post('login-customer','LoginController@login')->name('login_customer');
    Route::get('logout', 'LoginController@logOut')->name('logout');
});
Route::group(['namespace'=>'Site'],function(){
    Route::get('/', 'HomeController@index')->name('site_home');
    Route::get('/register-customer', 'CustomerController@register')->name('register');
    Route::resource('customer', 'CustomerController');
    //product ajax
    Route::get('/detail-product', 'ProductController@getDetaiProduct')->name('detail_product');
    Route::post('/addcart-product', 'ProductController@addToCart')->name('add_to_cart');
    //product
    Route::get('product/{slug}', 'ProductController@getDetailProduct')->name('get_detail_product');
    Route::get('category/{slug}', 'ProductController@getProductsCategory')->name('get_category_product');
    Route::post('product/review', 'ProductController@addReview')->name('add_review');

    // cart
    Route::get('/my-cart', 'OrderController@showCart')->name('my_cart');
    Route::get('/remove-item-cart', 'ProductController@removeItemCart')->name('remove_item_cart');
    Route::get('/update-item-cart', 'ProductController@updateItemCart')->name('update_item_cart');
    Route::get('/get-total-price', 'ProductController@getTotalPrice')->name('get_total_price');
    // checkout
    Route::get('/check-out', 'OrderController@showPayMent')->name('check_out');
    Route::post('/check-out', 'OrderController@createOrder')->name('create_order');
    Route::get('/update-cart', 'OrderController@updateCartHeader')->name('update_cart');

});
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
