<?php
/**
 * Created by PhpStorm.
 * User: Nam Moma
 * Date: 2/18/2020
 * Time: 4:03 PM
 */

namespace App\Ultility;


use Illuminate\Support\Facades\Storage;

class AntiAttackForm
{
    public function isUnderAttack () {
        try {
            // lấy ra ip của người dùng
            $userIp = $_SERVER['REMOTE_ADDR'];

            // check xem có tồn tại file ip không
            $exists = Storage::disk('local')->exists('ip_attack.txt');

            if ($exists) {
                $contents = Storage::disk('local')->get('ip_attack.txt');
                // nếu tấn công quá 4 lần
                $countIp = substr_count($contents, $userIp);
                if ($countIp > 4) {

                    Storage::disk('local')->append('ip_attack_five.txt', $userIp);
                    return;
                }
                // nếu không quá 4 lần thì lưu lại
                Storage::disk('local')->append('ip_attack.txt', $userIp);

                return;
            }


            Storage::disk('local')->append('ip_attack.txt', $userIp);

            return ;
        } catch (\Exception $e) {
            return ;
        }
    }
}
