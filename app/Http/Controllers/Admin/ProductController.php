<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Category;
use App\Entity\CategoryPost;
use App\Entity\Product;
use App\Entity\Post;
use App\Http\Controllers\Controller;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category();
        $categories = $category->getCategory();
        return view('admin.products.add',compact('categories'));
    }
    public function createProduct(Request $request){
        $category = new Category();
        $categories = $category->getCategory();
        return view('admin.products.add',compact('categories'));
    }
    public function getChildCategory(Request $request){
        $childCate = Category::where('parent',$request->input('cateID'))->get();
        return $childCate;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validateAddProduct($request);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
            $imageAvatar = $request->file('image');
            $image = $imageAvatar->getClientOriginalName();
//            $imageAvatar->move(public_path('image_upload/images'), $image);

        $image_resize = Image::make($imageAvatar->getRealPath());
        $image_resize->resize(600, 800);
        $image_resize->save(public_path('image_upload/images/'. $image));


        $slug = $this->createSlug($request);
        $postId = $this->createPost($request,$slug,$image);
         $this->createProducts($request,$postId);
         $this->createCatePost($request,$postId);
        return redirect()->route('product.create');
    }
    private function createPost(Request $request,$slug,$image){
        $post = new Post();
        $postId = $post->insertGetId([
            'title'=>$request->input('title'),
            'description'=>$request->input('description'),
            'post_type'=>'product',
            'image'=>$image,
            'created_at'=>new \DateTime(),
            'updated_at'=>new \DateTime()
        ]);
        // insert slug
        $postWithSlug = $post->where('slug', $slug)
            ->where('post_type', 'product')
            ->first();
        if (empty($postWithSlug)) {
            $post->where('post_id', '=', $postId)
                ->update([
                    'slug' => $slug
                ]);
        } else {
            $post->where('post_id', '=', $postId)
                ->update([
                    'slug' => $slug.'-'.$postId
                ]);
        }
        return $postId;
    }
    private function createProducts(Request $request,$postId){
        $product = new Product();
        $productId = $product->insertGetId([
            'code'=>$request->input('code'),
            'post_id'=>$postId,
            'price'=> str_replace('.', '', $request->input('price')),
            'discount'=> str_replace('.', '', $request->input('discount')),
            'discount_start'=>$request->input('discount_start'),
            'discount_end'=>$request->input('discount_end'),
            'material'=>$request->input('material'),
            'created_at'=>new \DateTime(),
            'updated_at'=>new \DateTime()
        ]);

        return $productId;
    }
    private function createCatePost(Request $request,$postId){
        $catePost = new CategoryPost();
        $catePost->insert([
            'post_id'=>$postId,
            'category_id'=>$request->input('parent_category'),
            'created_at'=>new \DateTime(),
            'updated_at'=>new \DateTime()
        ]);
    }
    protected function createSlug($request) {
        try {
            // if slug null slug create as title
            $slug = $request->input('slug');
            if (empty($slug)) {
                $slug = Ultility::createSlug($request->input('title'));
            }
        } catch (\Exception $e) {
            $slug = rand(10,10000000);

        } finally {
            return $slug;
        }
    }
    private function validateAddProduct(Request $request){
        $messages = [
            "title.required" => "Title is required",
            "parent_category.required" => "Danh mục is required",
            "code.required" => "Code product is required",
            "price.required" => "Price is required",
            "image.email" => "Image Product is not valid",

        ];
        return $validator = Validator::make($request->all(), [
            'title' => 'required',
            'parent_category' => 'required',
            'code' => 'required',
            'price' => 'required',
            'image' => 'required',
        ], $messages);

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
