<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Category;
use App\Http\Controllers\Controller;
use App\Ultility\Error;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category();
        $categories = $category->getCategory();
        return view('admin.categories.add',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // if slug null slug create as title
        $slug = $this->createSlug($request);
        // insert to database
        $cateId = $this->insertCategory($request, $slug);
        return redirect()->route('category.create');
    }
    protected function createSlug($request) {
        try {
            // if slug null slug create as title
            $slug = $request->input('slug');
            if (empty($slug)) {
                $slug = Ultility::createSlug($request->input('title'));
            }
        } catch (\Exception $e) {
            $slug = rand(10,10000000);

        } finally {
            return $slug;
        }
    }
    private function insertCategory($request, $slug) {
        try {
            $category = new Category();
            $cateId = $category->insertGetId([
                'title' => $request->input('title'),
                'parent' => $request->input('parent'),
                'post_type' => 'product',
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),

            ]);

            // insert slug
            $cateWithSlug = $category->where('slug', $slug)
                ->where('post_type', 'product')
                ->first();
            if (empty($cateWithSlug)) {
                $category->where('category_id', '=', $cateId)
                    ->update([
                        'slug' => $slug
                    ]);
            } else {
                $category->where('category_id', '=', $cateId)
                    ->update([
                        'slug' => $slug.'-'.$cateId
                    ]);
            }

            return $cateId;
        } catch (\Exception $e) {
//            Error::setErrorMessage('Lỗi xảy ra khi thêm mới danh mục: dữ liệu nhập vào không hợp lệ.');
//
//            Log::error('http->admin->CategoryController->insertCategory: Lỗi insert danh mục category');

            return 1;
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
