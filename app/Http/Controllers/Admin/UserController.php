<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\User;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Validator;
class UserController extends Controller
{
    public function showCreateUser(){
        return view('admin.users.add');
    }
    public function createUser(Request $request){
        $validator = $this->validateRegister($request);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        User::insert([
            "email"=>$request->input('email'),
            "password"=>bcrypt($request->input('password')),
            "name"=>$request->input('name'),
            "phone"=>$request->input('phone'),
            "role"=>3,
            "created_at"=> new \DateTime(),
            "updated_at"=> new \DateTime(),
        ]);
        return redirect()->back()->with('status', 'Thêm mới tài khoản thành công');

    }
    private function validateRegister(Request $request){
        $messages = [
            "email.required" => "Email is required",
            "email.email" => "Email is not valid",
            "email.unique" => "Email registered. Pls Choose other email",
            "name.required" => "Name is required",
            "phone.required" => "Phone is required",
            "password.required" => "Password is required",
            "password.min" => "Password must be at least 6 characters"
        ];
        return $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'name' => 'required',
            'phone' => 'required'
        ], $messages);

    }

}
