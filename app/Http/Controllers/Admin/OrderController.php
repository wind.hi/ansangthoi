<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Category;
use App\Entity\CategoryPost;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Entity\Post;
use App\Http\Controllers\Controller;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.orders.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::where('order_id',$id)->first();
        return view('admin.orders.detail',compact('order'));
    }
    public function detailOrder(){
        return view('admin.orders.detail');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function anyDatatable(Request $request){
        $orders = Order::select(
            'order_id',
            'customer_name',
            'order_type',
            'address_shipping',
            'phone',
            'date_order',
            'total_price'
        );
        return Datatables::of($orders)
            ->addColumn('action', function ($order){
                $string = '<a href="' . route('order.edit', $order->order_id) .'">
                                <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                           </a>';
                return $string;
            })
            ->orderColumn('orders.order_id', 'orders.order_id desc')
            ->make(true);
    }
    public function anyDatatableOrderItem($id){
       $items = OrderItem::where('order_id',$id)->select(
           'order_id',
           'product_name',
           'price',
           'quantity'
       );
        return Datatables::of($items)
            ->orderColumn('items.order_item_id', 'items.order_item_id desc')
            ->make(true);
    }
    public function updateStatus(Request $request){
        if($request->input('status') == 0){
            Order::where('order_id',$request->input('order_id'))
                ->update([
                    "status"=>$request->input('status')
                ]);
            $orderId = Order::where('order_id',$request->input('order_id'))->first()->order_id;
            $string = '';
            $string .= ' <a href="#" class="btn btn-secondary btn-icon-split" onclick="return updateStatus('.$orderId.',1)" >
                                <span class="text">Đang chờ xác nhận</span>
                            </a>';
            return $string;
        }
        if($request->input('status') == 1){
            Order::where('order_id',$request->input('order_id'))
                ->update([
                    "status"=>$request->input('status')
                ]);
            $orderId = Order::where('order_id',$request->input('order_id'))->first()->order_id;
            $string = '';
             $string .= ' <a href="#" class="btn btn-info btn-icon-split" onclick="return updateStatus('.$orderId.',2)" >
                                <span class="text">Đã xác nhận</span>
                            </a>
                            &nbsp;
                                <a onclick="return updateStatus('.$orderId.',0)" ><span class="text">về ban đầu</span></a>
                            ';
             return $string;
        }
        if($request->input('status') == 2){
            Order::where('order_id',$request->input('order_id'))
                ->update([
                    "status"=>$request->input('status')
                ]);
            $orderId = Order::where('order_id',$request->input('order_id'))->first()->order_id;
            $string = '';
            $string .= ' <a href="#" class="btn btn-success btn-icon-split" onclick="return updateStatus('.$orderId.',3)" >
                                <span class="text">Đã giao hàng</span>
                            </a>
                              &nbsp;
                                <a onclick="return updateStatus('.$orderId.',0)" ><span class="text">về ban đầu</span></a>

                            ';
            return $string;
        }
        if($request->input('status') == 3){
            Order::where('order_id',$request->input('order_id'))
                ->update([
                    "status"=>$request->input('status')
                ]);
            $orderId = Order::where('order_id',$request->input('order_id'))->first()->order_id;
            $string = '';
            $string .= ' <a href="#" class="btn btn-danger btn-icon-split" onclick="" >
                                <span class="text">Đã hủy đơn</span>
                            </a>
                              &nbsp;
                                <a onclick="return updateStatus('.$orderId.',0)" ><span class="text">về ban đầu</span></a>

                            ';
            return $string;
        }
    }
}
