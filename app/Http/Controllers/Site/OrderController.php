<?php

namespace App\Http\Controllers\Site;

use App\Entity\Customer;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Store;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Category;
use App\Entity\CategoryPost;
use App\Entity\Product;
use App\Entity\Post;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Session;
class OrderController extends Controller
{
    public function showCart(){
        return view('site.customer.cart');
    }
    public function showPayMent(){
        $stores = Store::select('store_id','store_name')->get();
        return view('site.customer.checkout',compact('stores'));
    }
    public function createOrder(Request $request){
        $validator = $this->validateAddOrder($request);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $orderId = $this->addOrder($request);
        $this->addOrderItem($request,$orderId);


    }
    private function validateAddOrder(Request $request){
        $messages = [
            "customer_name.required" => "Name is required",
            "receive.required" => "Hinh thuc nhan is required",
            "address.required" => "address is required",
            "phone.required" => "phone  is required ",
            "date_order.required" => "date_order is required",

        ];
        return $validator = Validator::make($request->all(), [
            'customer_name' => 'required',
            'receive' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'date_order' => 'required',
        ], $messages);

    }
    private function addOrder(Request $request){
        if($request->input('receive') == 'atshop'){
            $addressShipping = Store::where('store_id',$request->input('store'))->first()->store_name;
        }
        if(Auth::check()){
            $customerID = Customer::where('customer_user_id',Auth::id())->first()->customer_id;

            return $orderID = Order::insertGetId([
                "customer_id"=>$customerID,
                "customer_name"=>$request->input('customer_name'),
                "address_shipping"=>isset($addressShipping) ? $addressShipping : $request->input('address'),
                "method"=>$request->input('receive'),
                "phone"=>$request->input('phone'),
                "date_order"=>$request->input('date_order'),
                "total_price"=>$request->input('total_price'),
                "status"=>0,
                "order_type"=>"thành viên",
                'created_at'=>new \DateTime(),
                'updated_at'=>new \DateTime()
            ]);
        }
        return $orderID = Order::insertGetId([
            "customer_name"=>$request->input('customer_name'),
            "address_shipping"=>isset($addressShipping) ? $addressShipping : $request->input('address'),
            "method"=>$request->input('receive'),
            "phone"=>$request->input('phone'),
            "date_order"=>$request->input('date_order'),
            "total_price"=>$request->input('total_price'),
            "status"=>0,
            "order_type"=>"khách",
            'created_at'=>new \DateTime(),
            'updated_at'=>new \DateTime()
        ]);
    }
    private function addOrderItem(Request $request,$orderId){
        if ($request->session()->has('cart_item')) {
            foreach (Session::get('cart_item') as $item){
                OrderItem::insert([
                    "order_id"=>$orderId,
                    "product_id"=>$item['id'],
                    "product_name"=>$item['product_name'],
                    "price"=>$item['price'],
                    "quantity"=>$item['quantity'],
                    'created_at'=>new \DateTime(),
                    'updated_at'=>new \DateTime()
                ]);
            }
        }
    }
    public function updateCartHeader(){
        if (!empty(Session::get('cart_item'))){
            $string = '';
            $items = Session::get('cart_item');
            $listItem = '';
            foreach ($items as $item){
                $listItem .= ' <tr>
                                    <td class="text-center"><a href="#"><img class="img-thumbnail" title="lorem ippsum dolor dummy" alt="lorem ippsum dolor dummy" src="'.asset('image_upload/images/'.$item['product_image']).'"  style="width: 50px; height:59px"></a></td>
                                    <td class="text-left"><a href="#">'.$item['product_name'].'</a></td>
                                    <td class="text-right">x '.$item['quantity'].'</td>
                                    <td class="text-right">'.$item['price'].'</td>
                                </tr>';
            }
            return $string .= '<div id="cart" class="btn-group btn-block">
                    <button type="button" class="btn btn-inverse btn-block btn-lg dropdown-toggle cart-dropdown-button"> <span id="cart-total"><span class="cart-title">Shopping Cart</span><br>
          '.count(Session::get('cart_item')).' item(s)</span> </button>
                    <ul class="dropdown-menu pull-right cart-dropdown-menu">
                        <li>
                            <table class="table table-striped">
                                <tbody>
                                    '.$listItem.'
                                </tbody>
                            </table>
                            <p class="text-right"> <span class="btn-viewcart"><a href="cart.html"><strong><i class="fa fa-shopping-cart"></i> View Cart</strong></a></span> <span class="btn-checkout"><a href="checkout.html"><strong><i class="fa fa-share"></i> Checkout</strong></a></span> </p>
                        </li>
                    </ul>
                </div>';
        }
    }
}
