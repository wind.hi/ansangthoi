<?php

namespace App\Http\Controllers\Site;

use App\Entity\RateProduct;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Category;
use App\Entity\CategoryPost;
use App\Entity\Product;
use App\Entity\Post;
use App\Ultility\Ultility;
use Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
//use Session;
session_start();
class ProductController extends Controller
{
    public function getDetaiProduct(Request $request){
        $output = '';
        $products = Product::leftJoin('posts','products.post_id','posts.post_id')
            ->leftJoin('category_post','posts.post_id','category_post.post_id')
            ->join('categories','categories.category_id','category_post.category_id')
            ->select(
                'products.product_id',
                'products.code',
                'products.price',
                'posts.title',
                'posts.description',
                'posts.image',
                'categories.title as category'
            )->where('product_id',$request->input('product_id'))->first();
          $price_format = number_format($products->price,0,',','.');

        $output .= ' <div class="row">
                    <div class="col-md-6 product_img">
                        <img src="'.asset('image_upload/images/'.$products->image).'" class="img-responsive">
                    </div>
                    <div class="col-md-6 product_content">
                        <h4 style="font-weight: bold;" >Sản phẩm: <span style="font-weight: normal">'.$products->title.'</span></h4>
                        <h4 style="font-weight: bold">Giá: <span style="font-weight: normal">'.$price_format.' VND</span></h4>
                        <h4 style="font-weight: bold">Danh mục: <span style="font-weight: normal">'.$products->category.'</span></h4>


                        <div class="btn-ground">

                            <form name="hihi" method="" action="">
                                <div class="input-groupnumber wow fadeInLeft" data-wow-delay="0.4s">

                                    <input type="hidden" name="product_id_modal" value=" '.$products->product_id.' "/>
                                    <input type="hidden" name="quantity" value="1"/>
                                    <input type="hidden" name="action" value="add"/>

                                </div>

                                <button type="button" class="btn btn-primary" name="add_to_cart" onclick="return addToCart(this);"><span class="fa fa-cart-plus"></span>&nbsp; Thêm vào giỏ</button>
                        </div>
                        </form>
                    </div>
                </div>';
        return $output;
    }
    public function addToCart(Request $request){
        $product = Product::findProductById( $request->input('product_id'));

        $id = trim($request->input('product_id'));

        if (!empty(Session::get('cart_item'))){
            $cart_items = Session::get('cart_item');
            if (isset($cart_items[$id]['id'])) {
                $quantity_exits = $cart_items[$id]['quantity'];
                $item = array();
                $item['id'] = $request->input('product_id');
                $item['product_name'] = $product->title;
                $item['product_image'] = $product->image;
                $item['product_category'] =  $product->category;
                $item['price'] = $product->price;
                $item['quantity'] = $quantity_exits + $request->input('quantity');
//                $cart_items[$id] = $item;
//                $items = array();
                $cart_items[$id] = $item;
                Session::put('cart_item', $cart_items);
            }
            else {
                $item = array();
                $item['id'] = $request->input('product_id');
                $item['product_name'] = $product->title;
                $item['product_image'] = $product->image;
                $item['product_category'] = $product->category;
                $item['price'] = $product->price;
                $item['quantity'] = $request->input('quantity');


                $cart_items[$id] = $item;
//                array_push($cart_items,$items);
                Session::put('cart_item', $cart_items);
            }

        }else {
            $item = array();
            $item['id'] = $product->product_id;
            $item['product_name'] = $product->title;
            $item['product_image'] = $product->image;
            $item['product_category'] = $product->category;
            $item['price'] = $product->price;
            $item['quantity'] =  $request->input('quantity');
            $items = array();
            $items[$id] = $item;
            Session::put('cart_item', $items);
        }
        return Session::get('cart_item');
    }
    public function removeItemCart(Request $request){
        $cart_items = Session::get('cart_item');
        unset($cart_items[$request->input('product_id')]);
        Session::put('cart_item', $cart_items);
        if(empty(Session::get('cart_item'))){
            return 0;
        }
        $listItem = '';
        foreach ( Session::get('cart_item') as $item){
            $listItem .= '<tr>
                            <td class="text-center"><a href="product.html"><img class="img-thumbnail" title="'.$item['product_name'].'" alt="'.$item['product_name'].'" src="'.asset('image_upload/images/'.$item['product_image']).'" style="width: 72px; height: auto;"></a></td>
                            <td class="text-left"><a href="product.html">'.$item['product_name'].'</a></td>
                            <td class="text-left">'.$item['product_category'].'</td>
                            <td class="text-left"><div style="max-width: 200px;" class="input-group btn-block">
                                    <input type="text" class="form-control quantity" size="1" value="'.$item['quantity'].'" name="quantity">
                                    <input type="hidden"  class="hidden_quantity"  value="'.$item['id'].'" >

                                    <span class="input-group-btn">
                    <button class="btn btn-primary" title="" data-toggle="tooltip" type="button" data-original-title="Update" data-id="'.$item['id'].'" onclick="return updateItem(this)"><i class="fa fa-refresh"></i></button>
                    <button class="btn btn-danger" title="" data-toggle="tooltip" type="button" data-original-title="Remove" data-id="'.$item['id'].'" onclick="return removeItem(this)"><i class="fa fa-times-circle"></i></button>
                    </span></div></td>
                            <td class="text-right">'.number_format($item['price'],'0',',','.').' vnđ</td>
                            <td class="text-right">'.number_format($item['price'] * $item['quantity'],'0',',','.').' vnđ</td>

                        </tr>';
        }
        $output = '';
        $output .= $listItem;
        return $output;
    }

    public function updateItemCart(Request $request){
        $cart_items = Session::get('cart_item');
        $cart_items[$request->input('product_id')]['quantity'] = $request->input('quantity');
        Session::put('cart_item', $cart_items);
        $listItem = '';
        $total = 0;
        foreach ( Session::get('cart_item') as $item){
            $listItem .= '<tr>
                            <td class="text-center"><a href="product.html"><img class="img-thumbnail" title="'.$item['product_name'].'" alt="'.$item['product_name'].'" src="'.asset('image_upload/images/'.$item['product_image']).'" style="width: 72px; height: auto;"></a></td>
                            <td class="text-left"><a href="product.html">'.$item['product_name'].'</a></td>
                            <td class="text-left">'.$item['product_category'].'</td>
                            <td class="text-left"><div style="max-width: 200px;" class="input-group btn-block">
                                    <input type="text" class="form-control quantity" size="1" value="'.$item['quantity'].'" name="quantity">
                                    <input type="hidden"  class="hidden_quantity"  value="'.$item['id'].'" >

                                    <span class="input-group-btn">
                    <button class="btn btn-primary" title="" data-toggle="tooltip" type="button" data-original-title="Update"  data-id="'.$item['id'].'" onclick="return updateItem(this)"><i class="fa fa-refresh"></i></button>
                    <button class="btn btn-danger" title="" data-toggle="tooltip" type="button" data-original-title="Remove" data-id="'.$item['id'].'" onclick="return removeItem(this)"><i class="fa fa-times-circle"></i></button>
                    </span></div></td>
                            <td class="text-right">'.number_format($item['price'],'0',',','.').' vnđ</td>
                            <td class="text-right">'.number_format($item['price'] * $item['quantity'],'0',',','.').' vnđ</td>


                        </tr>';
        }
        $output = '';
        $output .= $listItem;
        return $output;
    }
    public function getTotalPrice(Request $request){
        $total = 0;
        foreach ( Session::get('cart_item') as $item){
           $total += $item['price'] * $item['quantity'];
        }
        return number_format($total,'0',',','.');
    }
    public function getDetailProduct($slug){
        $product = Product::leftJoin('posts','products.post_id','posts.post_id')
            ->leftJoin('category_post','posts.post_id','category_post.post_id')
            ->join('categories','categories.category_id','category_post.category_id')
            ->select(
                'products.product_id',
                'products.code',
                'products.price',
                'posts.title',
                'posts.post_id',
                'posts.description',
                'posts.image',
                'categories.title as category'
            )->where('posts.slug',$slug)->first();
        $reviewsId =  RateProduct::where('post_id',$product->post_id)
            ->select(array(DB::raw('MAX(review_product_id) as id')))->groupBy('customer_name')
            ->orderBy('id','desc')->get();
        $reviews = RateProduct::where('post_id',$product->post_id)->whereIn('review_product_id',$reviewsId)
            ->select('post_id','customer_name','content','created_at')
            ->orderBy('review_product_id','desc')
            ->get();
        return view('site.products.detail_product',compact('product','reviews'));
    }
    public function getProductsCategory($slug){
        $products = Product::leftJoin('posts','products.post_id','posts.post_id')
            ->leftJoin('category_post','posts.post_id','category_post.post_id')
            ->join('categories','categories.category_id','category_post.category_id')
            ->select(
                'products.product_id',
                'products.code',
                'products.price',
                'products.discount',
                'posts.slug',
                'posts.title',
                'posts.description',
                'posts.image',
                'categories.slug as cate',
                'categories.title as category'
            )->where('categories.slug',$slug)->get();
        return view('site.products.category_product',compact('products','slug'));
    }
    public function addReview(Request $request){
        RateProduct::insert([
           "post_id"=>$request->input('post_id'),
           "customer_name"=>$request->input('customer_name'),
           "content"=>$request->input('content'),
            "created_at"=> new \DateTime(),
            "updated_at"=> new \DateTime(),
        ]);
      $reviewsId =  RateProduct::where('post_id',$request->input('post_id'))
                    ->select(array(DB::raw('MAX(review_product_id) as id')))->groupBy('customer_name')
                    ->orderBy('id','desc')->get();
      $reviews = RateProduct::where('post_id',$request->input('post_id'))->whereIn('review_product_id',$reviewsId)
          ->select('post_id','customer_name','content','created_at')
          ->orderBy('review_product_id','desc')
          ->get();
        $string = '';
        foreach ($reviews as $review){
            $string .= '  <ul class="blog-meta">
                                <li><i class="fa fa-user"></i><span><a rel="author" title="Posts by Admin" href="#">'.$review->customer_name.'</a></span></li>
                                <li><i class="fa fa-clock-o"></i><span class="dt-published">December 23, 2015</span></li>
                            </ul>
                            <p class="p-summary"></p>
                            <p>'.$review->content.'</p>
                            <hr/>';
        }
        return $string;
    }
}
