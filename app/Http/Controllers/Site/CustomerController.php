<?php

namespace App\Http\Controllers\Site;

use App\Entity\Customer;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Validator;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function register(){
        return view('site.register.register');
    }
    public function createUser(Request $request){

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validateRegister($request);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $userId =  $this->insertUser($request);
        $this->insertCustomer($request,$userId);
        return redirect()->back()->with('status', 'Đăng ký thành công');

    }
    private function validateRegister(Request $request){
        $messages = [
            "email.required" => "Email is required",
            "email.email" => "Email is not valid",
            "email.unique" => "Email registered. Pls Choose other email",
            "customer_name.required" => "Name is required",
            "nick_name.required" => "Nick name is required",
            "address.required" => "Phone is required",
            "password.required" => "Password is required",
            "password.min" => "Password must be at least 6 characters"
        ];
        return $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users',
            'password' => 'required|min:6',
            'customer_name' => 'required',
            'address' => 'required',
            'nick_name' => 'required'
        ], $messages);

    }
    private function insertUser(Request $request){
        $userId =  User::insertGetId([
            "email"=>$request->input('email'),
            "password"=>bcrypt($request->input('password')),
            "name"=>$request->input('customer_name'),
            "role"=>1,
            "created_at"=> new \DateTime(),
            "updated_at"=> new \DateTime(),
        ]);
        return $userId;
    }
    private function insertCustomer(Request $request, $userId){
        Customer::insert([
           "customer_name"=>$request->input('customer_name'),
           "customer_user_id"=>$userId,
           "email"=>$request->input('email'),
           "nick_name"=>$request->input('nick_name'),
           "address"=>$request->input('address'),
            "created_at"=> new \DateTime(),
            "updated_at"=> new \DateTime(),
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
