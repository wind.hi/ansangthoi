<?php

namespace App\Http\Controllers\Site;

use App\Entity\OrderItem;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Category;
use App\Entity\CategoryPost;
use App\Entity\Product;
use App\Entity\Post;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\DB;

use Validator;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productId= array();
        $productBestSaleID = OrderItem::select(array('product_id', DB::raw('COUNT(order_item.product_id) as total')))
                    ->groupBy('product_id')->orderBy('total','desc')->limit(6)->get();
        if($productBestSaleID->count() > 0){
            foreach ($productBestSaleID as $product){
                $productId[]=$product->product_id;
            }
        }
        $productBestSale = Product::leftJoin('posts','products.post_id','posts.post_id')
            ->leftJoin('category_post','posts.post_id','category_post.post_id')
            ->join('categories','categories.category_id','category_post.category_id')
            ->select(
                'products.product_id',
                'products.code',
                'products.price',
                'posts.title',
                'posts.slug',
                'posts.description',
                'posts.image',
                'categories.title as category'
            )
            ->whereIn('products.product_id',$productId)
            ->orderBy('products.product_id','desc')->get();

        $productDiscount = Product::leftJoin('posts','products.post_id','posts.post_id')
            ->leftJoin('category_post','posts.post_id','category_post.post_id')
            ->join('categories','categories.category_id','category_post.category_id')
            ->select(
                'products.product_id',
                'products.discount',
                'products.code',
                'products.price',
                'posts.title',
                'posts.slug',
                'posts.description',
                'posts.image',
                'categories.title as category'
            )
            ->where('discount','>',0)
            ->orderBy('products.product_id','desc')->get();

        $productCombo = Product::leftJoin('posts','products.post_id','posts.post_id')
            ->leftJoin('category_post','posts.post_id','category_post.post_id')
            ->join('categories','categories.category_id','category_post.category_id')
            ->select(
                'products.product_id',
                'products.discount',
                'products.code',
                'products.price',
                'posts.title',
                'posts.slug',
                'posts.description',
                'posts.image',
                'categories.title as category'
            )
            ->where('category_post.category_id',9)
            ->orderBy('products.product_id','desc')->get();
        return view('site.home.index',compact('productBestSale','productDiscount','productCombo','productId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
