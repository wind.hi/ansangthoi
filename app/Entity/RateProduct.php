<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class RateProduct extends Model
{
    protected $table = 'review_product';

    protected $primaryKey = 'review_product_id';

    protected $fillable = [
        'review_product_id',
        'post_id',
        'customer_name',
        'content',
        'rate',
        'created_at',
        'updated_at'
    ];
}
