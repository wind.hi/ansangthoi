<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $primaryKey = 'product_id';

    protected $fillable = [
        'product_id',
        'code',
        'post_id',
        'price',
        'discount',
        'price_deal',
        'cost',
        'wholesale',
        'discount_start',
        'discount_end',
        'image_list',
        'filter',
        'properties',
        'buy_together',
        'deleted_at',
        'buy_after'
    ];
    public static function findProductById($id){
        $product = static::leftJoin('posts','products.post_id','posts.post_id')
            ->leftJoin('category_post','posts.post_id','category_post.post_id')
            ->join('categories','categories.category_id','category_post.category_id')
            ->select(
                'products.product_id',
                'products.code',
                'products.price',
                'posts.title',
                'posts.description',
                'posts.image',
                'categories.title as category'
            )->where('product_id',$id)->first();
        if (!empty($product)){
            return $product;
        }
        return null;
    }
}
