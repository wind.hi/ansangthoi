<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Category extends Model
{
    protected $table = 'categories';

    protected $primaryKey = 'category_id';

    protected $fillable = [
        'category_id',
        'title',
        'slug',
        'description',
        'image',
        'parent',
        'post_type',
        'meta_title',
        'meta_description',
        'meta_keyword',
        'deleted_at',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    public function getCategory( $postType = 'product'){
        try {
            $categories = $this->where([
                'post_type' => $postType,
            ])->where('parent', 0)
                ->orderBy('category_id')->get();

        } catch (\Exception $e) {
            $categories = array();

            Log::error('Entity->Category->getCategory: Lấy danh mục ra bị lỗi: '.$e->getMessage());
        } finally {
            return $categories;
        }
    }
}
