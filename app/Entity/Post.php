<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'posts';

    protected $primaryKey = 'post_id';

    protected $fillable = [
        'post_id',
        'title',
        'slug',
        'description',
        'tags',
        'content',
        'template',
        'image',
        'post_type',
        'visiable',
        'meta_title',
        'meta_description',
        'meta_keyword',
        'product_list',
        'views',
        'submit_form',
        'deleted_at',
        'created_at',
        'updated_at',
        'form_id'
    ];


}
