<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $primaryKey = 'order_id';

    protected $fillable = [
        'order_id',
        'customer_id',
        'customer_name',
        'address_shipping',
        'method',
        'phone',
        'order_type',
        'total_price',
        'status',
        'created_at',
        'updated_at'
    ];

}
