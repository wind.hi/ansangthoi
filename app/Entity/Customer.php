<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;

class Customer extends Model
{
    protected $table = 'customers';
    protected $primaryKey = 'customer_id';



    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */


    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_user_id',
        'customer_name',
        'address',
        'phone',
        'email',
        'image',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
