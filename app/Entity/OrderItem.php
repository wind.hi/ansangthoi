<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_item';

    protected $primaryKey = 'order_item_id';

    protected $fillable = [
        'order_item_id',
        'order_id',
        'product_id',
        'quantity',
        'price',
        'product_name',
        'created_at',
        'updated_at'
    ];
}
