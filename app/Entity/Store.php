<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'store';

    protected $primaryKey = 'store_id';

    protected $fillable = [
        'store_id',
        'store_name',
        'address',
        'phone',
        'image',
        'created_at',
        'updated_at'
    ];
}
